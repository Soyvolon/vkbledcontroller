﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Extensions;

namespace VKBLEDController.GUI.Config.Wrappers;

/// <summary>
/// Wraps a <see cref="ComboBox"/> in a handler that
/// displays the values of an enum.
/// </summary>
/// <typeparam name="T">The type of enum to display.</typeparam>
public class EnumWrapper<T> : IDisposable where T : System.Enum
{
    private bool disposedValue;

    public delegate void ValueChanged(object sender, T? value);
    public event ValueChanged? ValueChangedEvent;

    /// <summary>
    /// The currently selected value.
    /// </summary>
    public T? SelectedValue { get; private set; }
    /// <summary>
    /// The combo box to wrap.
    /// </summary>
    private ComboBox Box { get; set; }

    public EnumWrapper(ComboBox box)
    {
        SelectedValue = default;
        Box = box;

        Configure();
    }

    /// <summary>
    /// Set the value of the combo box.
    /// </summary>
    /// <param name="value">The enum value to set.</param>
    public void SetValue(T value)
    {
        var desc = value.GetDescription();

        Box.SelectedItem = Box.Items.IndexOf(desc);
        Box.Text = desc;
        Box.Refresh();
    }

    /// <summary>
    /// Configures the <see cref="ComboBox"/>.
    /// </summary>
    private void Configure()
    {
        Box.Items.Clear();

        foreach (T item in System.Enum.GetValues(typeof(T)))
        {
            var desc = item.GetDescription();
            Box.Items.Add(desc);
        }

        Box.SelectedIndexChanged += Box_SelectedIndexChanged;
    }

    /// <summary>
    /// Fires when the combo box value is changed.
    /// </summary>
    /// <param name="sender">The object sending the event.</param>
    /// <param name="e">The event arguments.</param>
    private void Box_SelectedIndexChanged(object? sender, EventArgs e)
    {
        if (Box.Items[Box.SelectedIndex] is string desc)
            SelectedValue = desc.GetValueFromDescription<T>();
        else SelectedValue = default;

        ValueChangedEvent?.Invoke(this, SelectedValue);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {

            }

            Box.SelectedIndexChanged -= Box_SelectedIndexChanged;
            Box = null!;

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

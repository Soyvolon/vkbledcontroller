﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Attributes;

namespace VKBLEDController.GUI.Config.Editors;
public static class EditorHelper
{
    private static Dictionary<Type, Type>? _editors = null;
    public static IReadOnlyDictionary<Type, Type> Editors
    {
        get
        {
            if (_editors is null)
            {
                var types = AssetLists.GetTypesWithAttribute<SettingsEditorAttribute>();
                _editors = [];
                foreach (var (type, attr) in types)
                    _editors.TryAdd(attr.TypeToEdit, type);
            }

            return _editors;
        }
    }
}

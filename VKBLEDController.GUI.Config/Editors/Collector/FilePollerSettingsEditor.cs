﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Polling;
using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Attributes;
using VKBLEDController.GUI.Config.Editors.Collector.Controls;

namespace VKBLEDController.GUI.Config.Editors.Collector;

[SettingsEditor(typeof(FilePollerSettings))]
public partial class FilePollerSettingsEditor : UserControl, IEditorGUIBase<FilePollerSettings>
{
    public FilePollerSettings? Settings { get; set; }

    public FilePollerSettingsEditor()
    {
        InitializeComponent();
    }

    public void SetSettingsInstance(object settings, IActivatable activatable)
    {
        if (settings is not FilePollerSettings s)
            return;

        Settings = s;
        ReloadPollFlow();
    }

    private void ReloadPollFlow()
    {
        if (Settings is null)
            return;

        pollFlow.Controls.Clear();
        foreach (var poll in Settings.PollingLocations)
            pollFlow.Controls.Add(new FilePollerLocationControl(poll, OnDeletePoll));
    }

    #region Events
    private void AddLocation_Click(object sender, EventArgs e)
    {
        if (Settings is null)
            return;

        Settings.PollingLocations.Add(new());
        ReloadPollFlow();
    }

    private void OnDeletePoll(FilePollerLocationSettings location)
    {
        if (Settings is null)
            return;

        Settings.PollingLocations.Remove(location);
        ReloadPollFlow();
    }
    #endregion
}

﻿namespace VKBLEDController.GUI.Config.Editors.Collector;

partial class HttpListenerSettingsEditor
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        addInstance = new Button();
        groupBox1 = new GroupBox();
        instanceFlow = new FlowLayoutPanel();
        groupBox1.SuspendLayout();
        SuspendLayout();
        // 
        // addInstance
        // 
        addInstance.Location = new Point(3, 3);
        addInstance.Name = "addInstance";
        addInstance.Size = new Size(155, 23);
        addInstance.TabIndex = 4;
        addInstance.Text = "Add Instance";
        addInstance.UseVisualStyleBackColor = true;
        addInstance.Click += AddInstance_Click;
        // 
        // groupBox1
        // 
        groupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        groupBox1.Controls.Add(instanceFlow);
        groupBox1.Location = new Point(3, 32);
        groupBox1.Name = "groupBox1";
        groupBox1.Size = new Size(482, 230);
        groupBox1.TabIndex = 3;
        groupBox1.TabStop = false;
        groupBox1.Text = "Http Listener Settings";
        // 
        // instanceFlow
        // 
        instanceFlow.AutoScroll = true;
        instanceFlow.Location = new Point(6, 22);
        instanceFlow.Name = "instanceFlow";
        instanceFlow.Size = new Size(470, 202);
        instanceFlow.TabIndex = 0;
        // 
        // HttpListenerSettingsEditor
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(addInstance);
        Controls.Add(groupBox1);
        Name = "HttpListenerSettingsEditor";
        Size = new Size(488, 273);
        groupBox1.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private Button addInstance;
    private GroupBox groupBox1;
    private FlowLayoutPanel instanceFlow;
}

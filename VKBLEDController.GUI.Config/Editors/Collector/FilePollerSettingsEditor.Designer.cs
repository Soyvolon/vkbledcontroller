﻿namespace VKBLEDController.GUI.Config.Editors.Collector;

partial class FilePollerSettingsEditor
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        groupBox1 = new GroupBox();
        pollFlow = new FlowLayoutPanel();
        openFileDiag = new OpenFileDialog();
        addLocation = new Button();
        groupBox1.SuspendLayout();
        SuspendLayout();
        // 
        // groupBox1
        // 
        groupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        groupBox1.Controls.Add(pollFlow);
        groupBox1.Location = new Point(3, 40);
        groupBox1.Name = "groupBox1";
        groupBox1.Size = new Size(482, 230);
        groupBox1.TabIndex = 1;
        groupBox1.TabStop = false;
        groupBox1.Text = "File Polling Settings";
        // 
        // pollFlow
        // 
        pollFlow.AutoScroll = true;
        pollFlow.Location = new Point(6, 22);
        pollFlow.Name = "pollFlow";
        pollFlow.Size = new Size(470, 202);
        pollFlow.TabIndex = 0;
        // 
        // openFileDiag
        // 
        openFileDiag.FileName = "openFileDialog1";
        // 
        // addLocation
        // 
        addLocation.Location = new Point(3, 11);
        addLocation.Name = "addLocation";
        addLocation.Size = new Size(155, 23);
        addLocation.TabIndex = 2;
        addLocation.Text = "Add Location";
        addLocation.UseVisualStyleBackColor = true;
        addLocation.Click += AddLocation_Click;
        // 
        // FilePollerSettingsEditor
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(addLocation);
        Controls.Add(groupBox1);
        Name = "FilePollerSettingsEditor";
        Size = new Size(488, 273);
        groupBox1.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion
    private GroupBox groupBox1;
    private OpenFileDialog openFileDiag;
    private Button addLocation;
    private FlowLayoutPanel pollFlow;
}

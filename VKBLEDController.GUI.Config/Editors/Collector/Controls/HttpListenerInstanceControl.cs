﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VKBLEDController.Config.Polling;
using VKBLEDController.Config.Http;

namespace VKBLEDController.GUI.Config.Editors.Collector.Controls
{
    public partial class HttpListenerInstanceControl : UserControl
    {
        private HttpListenerInstanceSettings Settings { get; init; }
        private Action<HttpListenerInstanceSettings> OnDeleteRequested { get; init; }

        public HttpListenerInstanceControl(HttpListenerInstanceSettings settings,
            Action<HttpListenerInstanceSettings> onDeleteRequested)
        {
            InitializeComponent();

            Settings = settings;
            OnDeleteRequested = onDeleteRequested;

            Populate();
        }

        private void Populate()
        {
            displayName.Text = Settings.Name;
            port.Value = Settings.Port;
        }

        #region Events
        private void DisplayName_TextChanged(object sender, EventArgs e)
            => Settings.Name = displayName.Text;

        private void Port_ValueChanged(object sender, EventArgs e)
            => Settings.Port = (int)port.Value;

        private void Delete_Click(object sender, EventArgs e)
            => OnDeleteRequested.Invoke(Settings);
        #endregion
    }
}

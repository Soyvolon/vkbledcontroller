﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Polling;

namespace VKBLEDController.GUI.Config.Editors.Collector.Controls;
public partial class FilePollerLocationControl : UserControl
{
    private FilePollerLocationSettings Settings { get; init; }
    private Action<FilePollerLocationSettings> OnDeleteRequested { get; init; }

    public FilePollerLocationControl(FilePollerLocationSettings settings,
        Action<FilePollerLocationSettings> onDeleteRequested)
    {
        InitializeComponent();

        Settings = settings;
        OnDeleteRequested = onDeleteRequested;

        Populate();
    }

    private void Populate()
    {
        pollSeconds.Value = (decimal)Settings.PollSeconds;
        pollFile.Text = Settings.PollFile;
        readAllLines.Checked = Settings.ReadAllLines;
        rollingFile.Checked = Settings.RollingFile;
        rollingFileRegex.Text = Settings.RollingFileRegex;
    }

    #region Events
    private void PollSeconds_ValueChanged(object sender, EventArgs e) 
        => Settings.PollSeconds = (int)pollSeconds.Value;

    private void PollFile_TextChanged(object sender, EventArgs e) 
        => Settings.PollFile = pollFile.Text;

    private void PickFileButton_Click(object sender, EventArgs e)
    {
        var res = openFileDiag.ShowDialog(this);
        if (res != DialogResult.OK)
        {
            return;
        }

        pollFile.Text = openFileDiag.FileName;
    }

    private void ReadAllLines_CheckedChanged(object sender, EventArgs e)
        => Settings.ReadAllLines = readAllLines.Checked;

    private void RollingFile_CheckedChanged(object sender, EventArgs e)
        => Settings.RollingFile = rollingFile.Checked;

    private void RollingFileRegex_TextChanged(object sender, EventArgs e)
        => Settings.RollingFileRegex = rollingFileRegex.Text;

    private void Delete_Click(object sender, EventArgs e)
        => OnDeleteRequested.Invoke(Settings);
    #endregion
}

﻿namespace VKBLEDController.GUI.Config.Editors.Collector.Controls;

partial class HttpListenerInstanceControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        filePollerLocationBox = new GroupBox();
        port = new NumericUpDown();
        delete = new Button();
        displayName = new TextBox();
        pollFileLabel = new Label();
        label1 = new Label();
        filePollerLocationBox.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)port).BeginInit();
        SuspendLayout();
        // 
        // filePollerLocationBox
        // 
        filePollerLocationBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        filePollerLocationBox.Controls.Add(port);
        filePollerLocationBox.Controls.Add(delete);
        filePollerLocationBox.Controls.Add(displayName);
        filePollerLocationBox.Controls.Add(pollFileLabel);
        filePollerLocationBox.Controls.Add(label1);
        filePollerLocationBox.Location = new Point(3, 3);
        filePollerLocationBox.Name = "filePollerLocationBox";
        filePollerLocationBox.Size = new Size(449, 110);
        filePollerLocationBox.TabIndex = 14;
        filePollerLocationBox.TabStop = false;
        filePollerLocationBox.Text = "Listener Instance";
        // 
        // port
        // 
        port.Location = new Point(119, 52);
        port.Maximum = new decimal(new int[] { 65535, 0, 0, 0 });
        port.Name = "port";
        port.Size = new Size(318, 23);
        port.TabIndex = 23;
        port.ValueChanged += Port_ValueChanged;
        // 
        // delete
        // 
        delete.Location = new Point(353, 79);
        delete.Name = "delete";
        delete.Size = new Size(84, 23);
        delete.TabIndex = 22;
        delete.Text = "Delete";
        delete.UseVisualStyleBackColor = true;
        delete.Click += Delete_Click;
        // 
        // displayName
        // 
        displayName.Location = new Point(119, 20);
        displayName.Name = "displayName";
        displayName.Size = new Size(318, 23);
        displayName.TabIndex = 16;
        displayName.TextChanged += DisplayName_TextChanged;
        // 
        // pollFileLabel
        // 
        pollFileLabel.Location = new Point(6, 50);
        pollFileLabel.Name = "pollFileLabel";
        pollFileLabel.Size = new Size(107, 23);
        pollFileLabel.TabIndex = 15;
        pollFileLabel.Text = "Port";
        pollFileLabel.TextAlign = ContentAlignment.MiddleRight;
        // 
        // label1
        // 
        label1.Location = new Point(6, 19);
        label1.Name = "label1";
        label1.Size = new Size(107, 23);
        label1.TabIndex = 14;
        label1.Text = "Display Name";
        label1.TextAlign = ContentAlignment.MiddleRight;
        // 
        // HttpListenerInstanceControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(filePollerLocationBox);
        Name = "HttpListenerInstanceControl";
        Size = new Size(455, 115);
        filePollerLocationBox.ResumeLayout(false);
        filePollerLocationBox.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)port).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private GroupBox filePollerLocationBox;
    private NumericUpDown port;
    private Button delete;
    private TextBox displayName;
    private Label pollFileLabel;
    private Label label1;
}

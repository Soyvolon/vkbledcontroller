﻿namespace VKBLEDController.GUI.Config.Editors.Collector.Controls;

partial class FilePollerLocationControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        filePollerLocationBox = new GroupBox();
        delete = new Button();
        label2 = new Label();
        rollingFileRegex = new TextBox();
        rollingFile = new CheckBox();
        readAllLines = new CheckBox();
        pickFileButton = new Button();
        pollFile = new TextBox();
        pollFileLabel = new Label();
        label1 = new Label();
        pollSeconds = new NumericUpDown();
        openFileDiag = new OpenFileDialog();
        filePollerLocationBox.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)pollSeconds).BeginInit();
        SuspendLayout();
        // 
        // filePollerLocationBox
        // 
        filePollerLocationBox.Controls.Add(delete);
        filePollerLocationBox.Controls.Add(label2);
        filePollerLocationBox.Controls.Add(rollingFileRegex);
        filePollerLocationBox.Controls.Add(rollingFile);
        filePollerLocationBox.Controls.Add(readAllLines);
        filePollerLocationBox.Controls.Add(pickFileButton);
        filePollerLocationBox.Controls.Add(pollFile);
        filePollerLocationBox.Controls.Add(pollFileLabel);
        filePollerLocationBox.Controls.Add(label1);
        filePollerLocationBox.Controls.Add(pollSeconds);
        filePollerLocationBox.Location = new Point(3, 3);
        filePollerLocationBox.Name = "filePollerLocationBox";
        filePollerLocationBox.Size = new Size(449, 162);
        filePollerLocationBox.TabIndex = 13;
        filePollerLocationBox.TabStop = false;
        filePollerLocationBox.Text = "Polling Location";
        // 
        // delete
        // 
        delete.Location = new Point(353, 133);
        delete.Name = "delete";
        delete.Size = new Size(84, 23);
        delete.TabIndex = 22;
        delete.Text = "Delete";
        delete.UseVisualStyleBackColor = true;
        delete.Click += Delete_Click;
        // 
        // label2
        // 
        label2.Location = new Point(6, 104);
        label2.Name = "label2";
        label2.Size = new Size(107, 22);
        label2.TabIndex = 21;
        label2.Text = "Rolling File Regex";
        label2.TextAlign = ContentAlignment.MiddleRight;
        // 
        // rollingFileRegex
        // 
        rollingFileRegex.Location = new Point(119, 105);
        rollingFileRegex.Name = "rollingFileRegex";
        rollingFileRegex.Size = new Size(318, 23);
        rollingFileRegex.TabIndex = 20;
        rollingFileRegex.TextChanged += RollingFileRegex_TextChanged;
        // 
        // rollingFile
        // 
        rollingFile.AutoSize = true;
        rollingFile.Location = new Point(353, 80);
        rollingFile.Name = "rollingFile";
        rollingFile.Size = new Size(84, 19);
        rollingFile.TabIndex = 19;
        rollingFile.Text = "Rolling File";
        rollingFile.UseVisualStyleBackColor = true;
        rollingFile.CheckedChanged += RollingFile_CheckedChanged;
        // 
        // readAllLines
        // 
        readAllLines.AutoSize = true;
        readAllLines.Location = new Point(248, 79);
        readAllLines.Name = "readAllLines";
        readAllLines.Size = new Size(99, 19);
        readAllLines.TabIndex = 18;
        readAllLines.Text = "Read All Lines";
        readAllLines.UseVisualStyleBackColor = true;
        readAllLines.CheckedChanged += ReadAllLines_CheckedChanged;
        // 
        // pickFileButton
        // 
        pickFileButton.Location = new Point(389, 50);
        pickFileButton.Name = "pickFileButton";
        pickFileButton.Size = new Size(48, 23);
        pickFileButton.TabIndex = 17;
        pickFileButton.Text = "Pick";
        pickFileButton.UseVisualStyleBackColor = true;
        pickFileButton.Click += PickFileButton_Click;
        // 
        // pollFile
        // 
        pollFile.Location = new Point(119, 50);
        pollFile.Name = "pollFile";
        pollFile.Size = new Size(264, 23);
        pollFile.TabIndex = 16;
        pollFile.TextChanged += PollFile_TextChanged;
        // 
        // pollFileLabel
        // 
        pollFileLabel.Location = new Point(6, 50);
        pollFileLabel.Name = "pollFileLabel";
        pollFileLabel.Size = new Size(107, 23);
        pollFileLabel.TabIndex = 15;
        pollFileLabel.Text = "Poll File";
        pollFileLabel.TextAlign = ContentAlignment.MiddleRight;
        // 
        // label1
        // 
        label1.Location = new Point(6, 19);
        label1.Name = "label1";
        label1.Size = new Size(107, 23);
        label1.TabIndex = 14;
        label1.Text = "Poll Seconds";
        label1.TextAlign = ContentAlignment.MiddleRight;
        // 
        // pollSeconds
        // 
        pollSeconds.DecimalPlaces = 2;
        pollSeconds.Increment = new decimal(new int[] { 5, 0, 0, 65536 });
        pollSeconds.Location = new Point(119, 21);
        pollSeconds.Name = "pollSeconds";
        pollSeconds.Size = new Size(318, 23);
        pollSeconds.TabIndex = 13;
        pollSeconds.ValueChanged += PollSeconds_ValueChanged;
        // 
        // FilePollerLocationControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(filePollerLocationBox);
        Name = "FilePollerLocationControl";
        Size = new Size(455, 170);
        filePollerLocationBox.ResumeLayout(false);
        filePollerLocationBox.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)pollSeconds).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private GroupBox filePollerLocationBox;
    private TextBox rollingFileRegex;
    private CheckBox rollingFile;
    private CheckBox readAllLines;
    private Button pickFileButton;
    private TextBox pollFile;
    private Label pollFileLabel;
    private Label label1;
    private NumericUpDown pollSeconds;
    private Label label2;
    private Button delete;
    private OpenFileDialog openFileDiag;
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Http;
using VKBLEDController.Config.Polling;
using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Attributes;
using VKBLEDController.GUI.Config.Editors.Collector.Controls;

namespace VKBLEDController.GUI.Config.Editors.Collector;

[SettingsEditor(typeof(HttpListenerSettings))]
public partial class HttpListenerSettingsEditor : UserControl, IEditorGUIBase<HttpListenerSettings>
{
    public HttpListenerSettingsEditor()
    {
        InitializeComponent();
    }

    public HttpListenerSettings? Settings { get; set; }

    public void SetSettingsInstance(object settings, IActivatable activatable)
    {
        if (settings is not HttpListenerSettings listenerSettings)
            return;

        Settings = listenerSettings;
        ReloadPollFlow();
    }

    private void ReloadPollFlow()
    {
        if (Settings is null)
            return;

        instanceFlow.Controls.Clear();
        foreach (var instance in Settings.Instances)
            instanceFlow.Controls.Add(new HttpListenerInstanceControl(instance, OnDeletePoll));
    }

    #region Events
    private void AddInstance_Click(object sender, EventArgs e)
    {
        if (Settings is null)
            return;

        Settings.Instances.Add(new ());
        ReloadPollFlow();
    }

    private void OnDeletePoll(HttpListenerInstanceSettings instance)
    {
        if (Settings is null)
            return;

        Settings.Instances.Remove(instance);
        ReloadPollFlow();
    }
    #endregion
}

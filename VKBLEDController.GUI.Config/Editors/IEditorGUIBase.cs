﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Util;

namespace VKBLEDController.GUI.Config.Editors;

/// <summary>
/// The typeless version of <see cref="IEditorGUIBase{T}"/>. Used
/// with reflection.
/// </summary>
public interface IEditorGUIBase
{
    /// <summary>
    /// Sets the settings object (T from the generic type) and the object that
    /// owns the <paramref name="settings"/>.
    /// </summary>
    /// <param name="settings">The settings object to edit.</param>
    /// <param name="activatable">The parent of the settings object.</param>
    public void SetSettingsInstance(object settings, IActivatable activatable);
}

/// <summary>
/// The interface for module settings GUI editors.
/// </summary>
/// <typeparam name="T">The type of settings this editor is for.</typeparam>
public interface IEditorGUIBase<T> : IEditorGUIBase
    where T : class
{
    /// <summary>
    /// The settings object to edit.
    /// </summary>
    public T? Settings { get; set; }
}

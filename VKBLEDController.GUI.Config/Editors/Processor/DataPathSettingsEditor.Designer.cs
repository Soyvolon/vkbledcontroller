﻿namespace VKBLEDController.GUI.Config.Editors.Processor;

partial class DataPathSettingsEditor
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        jpathBox = new GroupBox();
        jpathFlow = new FlowLayoutPanel();
        addJpath = new Button();
        jpathBox.SuspendLayout();
        SuspendLayout();
        // 
        // jpathBox
        // 
        jpathBox.Controls.Add(jpathFlow);
        jpathBox.Location = new Point(3, 36);
        jpathBox.Name = "jpathBox";
        jpathBox.Size = new Size(471, 232);
        jpathBox.TabIndex = 0;
        jpathBox.TabStop = false;
        jpathBox.Text = "Path Expressions";
        // 
        // jpathFlow
        // 
        jpathFlow.Location = new Point(6, 22);
        jpathFlow.Name = "jpathFlow";
        jpathFlow.Size = new Size(459, 204);
        jpathFlow.TabIndex = 0;
        // 
        // addJpath
        // 
        addJpath.Location = new Point(3, 7);
        addJpath.Name = "addJpath";
        addJpath.Size = new Size(136, 23);
        addJpath.TabIndex = 1;
        addJpath.Text = "Add Path";
        addJpath.UseVisualStyleBackColor = true;
        addJpath.Click += AddJpath_Click;
        // 
        // DataPathSettingsEditor
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(addJpath);
        Controls.Add(jpathBox);
        Name = "DataPathSettingsEditor";
        Size = new Size(477, 271);
        jpathBox.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private GroupBox jpathBox;
    private Button addJpath;
    private FlowLayoutPanel jpathFlow;
}

﻿namespace VKBLEDController.GUI.Config.Editors.Processor.Controls;

partial class ProcessorVariableControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        variableBox = new GroupBox();
        delete = new Button();
        variableName = new TextBox();
        variableBox.SuspendLayout();
        SuspendLayout();
        // 
        // variableBox
        // 
        variableBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        variableBox.Controls.Add(delete);
        variableBox.Controls.Add(variableName);
        variableBox.Location = new Point(3, 3);
        variableBox.Name = "variableBox";
        variableBox.Size = new Size(374, 46);
        variableBox.TabIndex = 0;
        variableBox.TabStop = false;
        variableBox.Text = "Variable Name";
        // 
        // delete
        // 
        delete.Location = new Point(300, 17);
        delete.Name = "delete";
        delete.Size = new Size(68, 23);
        delete.TabIndex = 1;
        delete.Text = "Delete";
        delete.UseVisualStyleBackColor = true;
        delete.Click += Delete_Click;
        // 
        // variableName
        // 
        variableName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        variableName.Location = new Point(6, 17);
        variableName.Name = "variableName";
        variableName.Size = new Size(288, 23);
        variableName.TabIndex = 0;
        variableName.TextChanged += VariableName_TextChanged;
        // 
        // ProcessorVariableControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(variableBox);
        Name = "ProcessorVariableControl";
        Size = new Size(380, 52);
        variableBox.ResumeLayout(false);
        variableBox.PerformLayout();
        ResumeLayout(false);
    }

    #endregion

    private GroupBox variableBox;
    private TextBox variableName;
    private Button delete;
}

﻿namespace VKBLEDController.GUI.Config.Editors.Processor.Controls;

partial class DataPathConfigControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        panel1 = new Panel();
        fromFile = new TextBox();
        label3 = new Label();
        label2 = new Label();
        label1 = new Label();
        delete = new Button();
        expression = new TextBox();
        variable = new ComboBox();
        booleanMatch = new TextBox();
        label4 = new Label();
        panel1.SuspendLayout();
        SuspendLayout();
        // 
        // panel1
        // 
        panel1.Controls.Add(booleanMatch);
        panel1.Controls.Add(label4);
        panel1.Controls.Add(fromFile);
        panel1.Controls.Add(label3);
        panel1.Controls.Add(label2);
        panel1.Controls.Add(label1);
        panel1.Controls.Add(delete);
        panel1.Controls.Add(expression);
        panel1.Controls.Add(variable);
        panel1.Location = new Point(3, 3);
        panel1.Name = "panel1";
        panel1.Size = new Size(422, 93);
        panel1.TabIndex = 0;
        // 
        // fromFile
        // 
        fromFile.Location = new Point(38, 33);
        fromFile.Name = "fromFile";
        fromFile.Size = new Size(179, 23);
        fromFile.TabIndex = 7;
        fromFile.TextChanged += FromFile_TextChanged;
        // 
        // label3
        // 
        label3.AutoSize = true;
        label3.Location = new Point(4, 37);
        label3.Name = "label3";
        label3.Size = new Size(28, 15);
        label3.TabIndex = 6;
        label3.Text = "SRC";
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new Point(3, 6);
        label2.Name = "label2";
        label2.Size = new Size(34, 15);
        label2.TabIndex = 4;
        label2.Text = "EXPR";
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(240, 36);
        label1.Name = "label1";
        label1.Size = new Size(28, 15);
        label1.TabIndex = 3;
        label1.Text = "VAR";
        // 
        // delete
        // 
        delete.Location = new Point(332, 61);
        delete.Name = "delete";
        delete.Size = new Size(87, 23);
        delete.TabIndex = 2;
        delete.Text = "Delete";
        delete.UseVisualStyleBackColor = true;
        delete.Click += Delete_Click;
        // 
        // expression
        // 
        expression.Location = new Point(38, 3);
        expression.Name = "expression";
        expression.Size = new Size(179, 23);
        expression.TabIndex = 1;
        expression.TextChanged += Expression_TextChanged;
        // 
        // variable
        // 
        variable.DropDownStyle = ComboBoxStyle.DropDownList;
        variable.FormattingEnabled = true;
        variable.Location = new Point(274, 32);
        variable.Name = "variable";
        variable.Size = new Size(145, 23);
        variable.TabIndex = 0;
        variable.SelectedIndexChanged += Variable_SelectedIndexChanged;
        // 
        // booleanMatch
        // 
        booleanMatch.Location = new Point(274, 3);
        booleanMatch.Name = "booleanMatch";
        booleanMatch.Size = new Size(145, 23);
        booleanMatch.TabIndex = 9;
        booleanMatch.TextChanged += BooleanMatch_TextChanged;
        // 
        // label4
        // 
        label4.AutoSize = true;
        label4.Location = new Point(223, 6);
        label4.Name = "label4";
        label4.Size = new Size(47, 15);
        label4.TabIndex = 8;
        label4.Text = "MATCH";
        // 
        // JPathConfigControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(panel1);
        Name = "JPathConfigControl";
        Size = new Size(428, 99);
        panel1.ResumeLayout(false);
        panel1.PerformLayout();
        ResumeLayout(false);
    }

    #endregion

    private Panel panel1;
    private Button delete;
    private TextBox expression;
    private ComboBox variable;
    private Label label2;
    private Label label1;
    private Label label3;
    private TextBox fromFile;
    private TextBox booleanMatch;
    private Label label4;
}

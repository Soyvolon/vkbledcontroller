﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Processors.DataPath;

namespace VKBLEDController.GUI.Config.Editors.Processor.Controls;
public partial class DataPathConfigControl : UserControl
{
    private DataPathConfig Config { get; init; }
    private ProcessorBase Processor { get; init; }
    private Action<DataPathConfig> OnDeleteRequested { get; init; }

    public DataPathConfigControl(DataPathConfig config, ProcessorBase processor,
        Action<DataPathConfig> onDeleteRequested)
    {
        InitializeComponent();

        Config = config;
        Processor = processor;
        OnDeleteRequested = onDeleteRequested;

        Populate();
    }

    private void Populate()
    {
        expression.Text = Config.Expression;
        fromFile.Text = Config.FromSourceRegex;
        booleanMatch.Text = Config.DataMatch;

        variable.Items.Clear();
        foreach (var varData in Processor.Settings.Variables)
            variable.Items.Add(varData.Name);
        variable.SelectedIndex = variable.Items.IndexOf(Config.VariableName);
    }

    #region Events
    private void Expression_TextChanged(object sender, EventArgs e)
        => Config.Expression = expression.Text;

    private void FromFile_TextChanged(object sender, EventArgs e)
        => Config.FromSourceRegex = fromFile.Text;

    private void BooleanMatch_TextChanged(object sender, EventArgs e)
        => Config.DataMatch = booleanMatch.Text;

    private void Variable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (variable.SelectedIndex == -1)
            Config.VariableName = "";
        else Config.VariableName = Processor.Settings.Variables[variable.SelectedIndex].Name;
    }

    private void Delete_Click(object sender, EventArgs e)
        => OnDeleteRequested.Invoke(Config);
    #endregion
}

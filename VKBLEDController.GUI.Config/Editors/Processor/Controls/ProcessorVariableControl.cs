﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Processors.Settings;

namespace VKBLEDController.GUI.Config.Editors.Processor.Controls;
public partial class ProcessorVariableControl : UserControl
{
    private ProcessorVariableConfig Variable { get; init; }

    private Action<ProcessorVariableConfig> OnDeleteRequested { get; init; }

    public ProcessorVariableControl(ProcessorVariableConfig variable,
        Action<ProcessorVariableConfig> onDeleteRequested)
    {
        InitializeComponent();
        Variable = variable;

        variableName.Text = Variable.Name;

        OnDeleteRequested = onDeleteRequested;
    }

    private void Delete_Click(object sender, EventArgs e)
        => OnDeleteRequested.Invoke(Variable);

    private void VariableName_TextChanged(object sender, EventArgs e)
        => Variable.Name = variableName.Text;
}

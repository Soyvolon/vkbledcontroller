﻿namespace VKBLEDController.GUI.Config.Editors.Processor;

partial class DataProcessorSettingsEditor
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        variableFlow = new FlowLayoutPanel();
        groupBox1 = new GroupBox();
        addVariable = new Button();
        groupBox1.SuspendLayout();
        SuspendLayout();
        // 
        // variableFlow
        // 
        variableFlow.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        variableFlow.AutoScroll = true;
        variableFlow.Location = new Point(6, 22);
        variableFlow.Name = "variableFlow";
        variableFlow.Size = new Size(459, 191);
        variableFlow.TabIndex = 0;
        // 
        // groupBox1
        // 
        groupBox1.Controls.Add(variableFlow);
        groupBox1.Location = new Point(3, 49);
        groupBox1.Name = "groupBox1";
        groupBox1.Size = new Size(471, 219);
        groupBox1.TabIndex = 1;
        groupBox1.TabStop = false;
        groupBox1.Text = "Variables";
        // 
        // addVariable
        // 
        addVariable.Location = new Point(9, 20);
        addVariable.Name = "addVariable";
        addVariable.Size = new Size(113, 23);
        addVariable.TabIndex = 2;
        addVariable.Text = "Add Variable";
        addVariable.UseVisualStyleBackColor = true;
        addVariable.Click += AddVariable_Click;
        // 
        // DataProcessorSettingsEditor
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(addVariable);
        Controls.Add(groupBox1);
        Name = "DataProcessorSettingsEditor";
        Size = new Size(477, 271);
        groupBox1.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private FlowLayoutPanel variableFlow;
    private GroupBox groupBox1;
    private Button addVariable;
}

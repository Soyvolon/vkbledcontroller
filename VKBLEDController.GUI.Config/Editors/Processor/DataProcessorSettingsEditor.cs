﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Processors.Settings;
using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Attributes;
using VKBLEDController.GUI.Config.Editors.Processor.Controls;

namespace VKBLEDController.GUI.Config.Editors.Processor;

[SettingsEditor(typeof(ProcessorSettings))]
public partial class DataProcessorSettingsEditor : UserControl, IEditorGUIBase<ProcessorSettings>
{
    public ProcessorSettings? Settings { get; set; }
    public ProcessorBase? Processor { get; set; }

    public DataProcessorSettingsEditor()
    {
        InitializeComponent();
    }

    public void SetSettingsInstance(object settings, IActivatable activatable)
    {
        if (settings is not ProcessorSettings s
            || activatable is not ProcessorBase p)
            return;

        Settings = s;
        Processor = p;

        // Do value setup
        ReloadVariableFlow();
    }

    private void ReloadVariableFlow()
    {
        if (Settings is null)
            return;

        variableFlow.Controls.Clear();
        foreach (var val in Settings.Variables)
            variableFlow.Controls.Add(new ProcessorVariableControl(val, OnDeleteVariable));
    }


    #region Events
    private void OnDeleteVariable(ProcessorVariableConfig variable)
    {
        if (Settings is null)
            return;

        Settings.Variables.Remove(variable);
        ReloadVariableFlow();
    }

    private void AddVariable_Click(object sender, EventArgs e)
    {
        Settings?.Variables.Add(new());
        ReloadVariableFlow();
    }
    #endregion
}

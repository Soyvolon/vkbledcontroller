﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Processors.DataPath;
using VKBLEDController.Config.Processors.DataPath.JSON;
using VKBLEDController.Config.Processors.DataPath.XML;
using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Attributes;
using VKBLEDController.GUI.Config.Editors.Processor.Controls;

namespace VKBLEDController.GUI.Config.Editors.Processor;

[SettingsEditor(typeof(JsonDataSettings))]
[SettingsEditor(typeof(XmlDataSettings))]
public partial class DataPathSettingsEditor : UserControl, IEditorGUIBase<IDataSettingsBase>
{
    public IDataSettingsBase? Settings { get; set; }
    public ProcessorBase? Processor { get; set; }

    public DataPathSettingsEditor()
    {
        InitializeComponent();
    }

    public void SetSettingsInstance(object settings, IActivatable activatable)
    {
        if (settings is not IDataSettingsBase s
            || activatable is not ProcessorBase p)
            return;

        Settings = s;
        Processor = p;

        ReloadJPathFlow();
    }

    private void ReloadJPathFlow()
    {
        if (Settings is null
            || Processor is null)
            return;

        jpathFlow.Controls.Clear();
        foreach (var jpath in Settings.GetConfigs())
            jpathFlow.Controls.Add(new DataPathConfigControl(jpath, Processor, OnJPathDelete));
    }

    #region Events
    private void AddJpath_Click(object sender, EventArgs e)
    {
        if (Settings is null)
            return;

        Settings.GetConfigs().Add(new());
        ReloadJPathFlow();
    }

    private void OnJPathDelete(DataPathConfig config)
    {
        if (Settings is null)
            return;

        Settings.GetConfigs().Remove(config);
        ReloadJPathFlow();
    }
    #endregion
}

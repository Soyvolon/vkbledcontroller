﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.GUI.Config.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class SettingsEditorAttribute(Type toEdit) : Attribute
{
    public Type TypeToEdit { get; set; } = toEdit;
}

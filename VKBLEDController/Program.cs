﻿using McMaster.Extensions.CommandLineUtils;

using Serilog;
using Serilog.Events;

using System;

using VKBLEDController.Config.Module;

namespace VKBLEDController;

class Program
{
    [Option("-m|--module", CommandOptionType.SingleValue, Description = "The module to run.")]
    public string ModulePath { get; set; } = "";

    public static int Main(string[] args)
        => CommandLineApplication.Execute<Program>(args);

    /// <summary>
    /// Entrypoint.
    /// </summary>
#pragma warning disable IDE0051 // is called via reflection from library
    private void OnExecute()
#pragma warning restore IDE0051 // Remove unused private members
    {
        if (string.IsNullOrWhiteSpace(ModulePath))
        {
            Console.WriteLine("A module is required to run. Pass the path to a module using the --module parameter.");
            Environment.Exit(1);
        }

        if (!Directory.Exists(ModulePath))
            ModulePath = Path.GetDirectoryName(ModulePath) ?? ModulePath;

        if (!Directory.Exists(ModulePath) || !File.Exists(Path.Join(ModulePath, ModuleSettings.CFG_FILE_NAME)))
        {
            Console.WriteLine("The provided module was not found.");
            Environment.Exit(1);
        }

        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            .CreateLogger();

        var settings = ModuleSettings.LoadAsync(ModulePath).GetAwaiter().GetResult();
        var runner = new ModuleRunner(settings);

        runner.StartAsync().GetAwaiter().GetResult();

        Task.Delay(-1).GetAwaiter().GetResult();
    }
}

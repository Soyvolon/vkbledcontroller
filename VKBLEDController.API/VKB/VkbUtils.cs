﻿using HidSharp;

using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.API.VKB;

/// <summary>
/// A set of utils for handling VKB HID devices.
/// </summary>
public static class VkbUtils
{
    /// <summary>
    /// VKB's vendor ID.
    /// </summary>
    public const int VKB_VENDOR_ID = 8989;

    /// <summary>
    /// Gets all VKB devices (those that match the VKB vendor ID) that are currently
    /// connected to the local computer.
    /// </summary>
    /// <remarks>
    /// Each <see cref="HidDevice"/> with a <see cref="HidDevice.VendorID"/> that matches <see cref="VKB_VENDOR_ID"/> is wrapped in a
    /// <see cref="VkbDevice"/> that handles LED interaction and some error handling when getting
    /// some device values (ex: the device name).
    /// <br /><br />
    /// The method also checks the feature report length to ensure the VKB virtual devices are not
    /// included in the list of available VKB devices. i.e, a device that is configured to have
    /// multiple virtual devices will only be have its physical device listed.
    /// </remarks>
    /// <returns>A <see cref="List{T}"/> of <see cref="VkbDevice"/>s.</returns>
    public static List<VkbDevice> GetVKBDevices()
    {
        // create our list ...
        List<VkbDevice> devices = [];
        // ... get all devices that match the vendor ID ...
        var vkbHidDevices = DeviceList.Local.GetHidDevices(vendorID:  VKB_VENDOR_ID);
        // ... and for each device ...
        foreach(var vkbDevice in vkbHidDevices)
        {
            // ... check that the feature reports exist ...
            if (vkbDevice.GetMaxFeatureReportLength() > 0)
                // ... then add the new VkbDevice ...
                devices.Add(new(vkbDevice));
        }

        Log.Debug("Got {count} VKB devices currently connected to the local computer.", devices.Count);

        // ... then return the list of devices.
        return devices;
    }
}

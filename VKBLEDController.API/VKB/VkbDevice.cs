﻿using HidSharp;
using HidSharp.Reports;

using Serilog;

using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.API.VKB.Config;

namespace VKBLEDController.API.VKB;

/// <summary>
/// A VKB device that supports LED lights.
/// </summary>
/// <param name="device">The VKB <see cref="HidDevice"/> instance.</param>
public class VkbDevice(HidDevice device)
{
    /// <summary>
    /// The max number of configurable LED settings.
    /// </summary>
    public const int LED_CONFIG_COUNT = 16;
    
    /// <summary>
    /// The report ID for configuring LEDs.
    /// </summary>
    public const byte LED_REPORT_ID = 0x59;
    /// <summary>
    /// The largest size of the feature report length.
    /// </summary>
    public readonly int LED_REPORT_LEN = device.GetMaxFeatureReportLength();

    /// <summary>
    /// The OP code to set LED configurations.
    /// </summary>
    private static readonly byte[] LED_SET_OP_CODE = [0x59, 0xA5, 0x0A];

    // Not used. Will remove later (2023-11-17).
    //private static readonly byte[] LED_SET_UKN_CODE = [0x6E, 0x4D, 0x34, 0x9A];

    /// <summary>
    /// The LED configurations that are locally saved (not on the device).
    /// </summary>
    /// <remarks>
    /// This array is cleared and re-populated from the device when
    /// <see cref="LoadLedConfig"/> is executed.
    /// </remarks>
    public List<LedConfig> Leds { get; private set; } = [];

    /// <summary>
    /// The name of the device.
    /// </summary>
    public string DeviceName
    {
        get
        {
            try
            {
                return device.GetFriendlyName();
            }
            catch
            {
                return "";
            }
        }
    }

    /// <summary>
    /// The name of the devices product.
    /// </summary>
    public string ProductName
    {
        get
        {
            try
            {
                return device.GetProductName();
            }
            catch
            {
                return "";
            }
        }
    }

    /// <summary>
    /// The product ID for the device.
    /// </summary>
    public int ProductId => device.ProductID;
    /// <summary>
    /// The vendor ID for the device.
    /// </summary>
    public int VendorId => device.VendorID;

    /// <summary>
    /// Load the LED config from the <see cref="device"/>.
    /// </summary>
    public void LoadLedConfig()
    {
        // Clear the existing leds ...
        Leds.Clear();

        // ... open a connection to the device ...
        using var stream = device.Open();

        // ... build the buffer ...
        byte[] buf = new byte[LED_REPORT_LEN];
        // ... set the report feature id ...
        buf[0] = LED_REPORT_ID;
        // ... and get the feature (populates the buffer) ...
        stream.GetFeature(buf);

        // ... if the first three values of the buffer do not equal
        // the set op code sequence, then no leds are set ...
        if (!buf.Take(3).SequenceEqual(LED_SET_OP_CODE))
        {
            // ... so return and do nothing more ...
            return;
        }

        // ... otherwise get the number of config entries ...
        var ledConfigNum = buf[7];
        // ... and the led data from the buffer ...
        var ledData = buf.Skip(8).ToArray();

        // ... then while there are configs left to parse ...
        while (ledConfigNum > 0)
        {
            // ... make sure we have four whole bytes ...
            if (ledData.Length < 4)
                break;

            // ... and take the next four bytes ...
            var data = ledData.Take(4).ToArray();
            // ... save them into a led config ...
            var led = new LedConfig(data);
            // ... then update the data array ...
            ledData = ledData.Skip(4).ToArray();

            // ... finally, add the led config to the Leds list ...
            Leds.Add(led);
            // ... and count down the remaining configs.
            ledConfigNum--;
        }

        Log.Debug("Loaded {count} LED configurations from {device} {deviceId}",
            Leds.Count, ProductName, (ProductId, VendorId));
    }

    /// <summary>
    /// Sets the data in <see cref="Leds"/> on the <see cref="device"/>.
    /// </summary>
    /// <exception cref="Exception"></exception>
    public void SetLedConfig()
    {
        // If we have too many leds ...
        if (Leds.Count > LED_CONFIG_COUNT)
            // ... throw an error ...
            throw new Exception($"Can only set a maximum of {LED_CONFIG_COUNT} LED configs.");

        // ... otherwise, get the config count ...
        var numConfigs = Leds.Count;
        // ... and all the config bytes ...
        byte[] ledConfigBytes = Leds.SelectMany(e => e.GetBytes()).ToArray();

        // ... then generate the two random bytes we need ...
        byte[] randomBytes = new byte[2];
        new Random().NextBytes(randomBytes);

        // ... then calculate the checksum ...
        byte[] checksum = CalculateChecksum(numConfigs, ledConfigBytes);

        // ... and compile the command to send to the device ...
        byte[] cmd = [
            // ... using the OP code ...
            .. LED_SET_OP_CODE,
            //.. LED_SET_UKN_CODE,
            // ... the checksum ...
            .. checksum,
            // ... the random bytes ...
            .. randomBytes,
            // ... the number of configs that exist ...
            .. new byte[] { (byte)numConfigs },
            // ... and the bytes that are the actual configs ...
            .. ledConfigBytes
        ];

        // ... if the command length is less than the max report length ...
        if (cmd.Length < LED_REPORT_LEN)
        {
            // ... then fill the remainder of the command with blank bytes ...
            cmd = [.. cmd, .. new byte[LED_REPORT_LEN - cmd.Length]];
        }

        // ... then open the device stream ...
        using var stream = device.Open();
        // ... and send the update to the device.
        stream.SetFeature(cmd);

        Log.Debug("Set {count} LED configurations on: {device} {deviceId}",
            Leds.Count, ProductName, (ProductId, VendorId));
    }

    /// <summary>
    /// Calculates the checksum for the led configurations.
    /// </summary>
    /// <param name="numConfigs">The number of configurations.</param>
    /// <param name="ledConfigBytes">The configured bytes.</param>
    /// <returns>A <see cref="byte[]"/> containing a two byte checksum.</returns>
    private static byte[] CalculateChecksum(int numConfigs, byte[] ledConfigBytes)
    {
        ushort checksum = 0xFFFF;

        // for each byte in the configs where each config contains three bytes ...
        for (int i = 0; i < (numConfigs + 1) * 3 && i < ledConfigBytes.Length; i++)
        {
            // build the checksum value
            checksum = ConfChecksumBit(checksum, ledConfigBytes[i]);
        }

        return BitConverter.GetBytes(checksum).Reverse().ToArray();
    }

    /// <summary>
    /// Configures the checksum bit.
    /// </summary>
    /// <param name="chk">The existing checksum.</param>
    /// <param name="b">The individual byte.</param>
    /// <returns>The <see cref="ushort"/> representing the checksum bit.</returns>
    private static ushort ConfChecksumBit(ushort chk, byte b)
    {
        chk ^= b;
        for (int i = 0; i < 8; i++)
        {
            ushort _ = (ushort)(chk & 1);
            chk >>= 1;
            if (_ != 0)
            {
                chk ^= 0xA001;
            }
        }
        return chk;
    }
}

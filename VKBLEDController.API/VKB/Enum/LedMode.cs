﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.API.VKB.Enum;
public enum LedMode
{
    /// <summary>
    /// Turn the LED off.
    /// </summary>
    [Description("Off")]
    Off = 0,
    /// <summary>
    /// Keep the LED constantly on.
    /// </summary>
    [Description("Constant")]
    Constant,
    /// <summary>
    /// Blink the LED slowly.
    /// </summary>
    [Description("Slow Blink")]
    SlowBlink,
    /// <summary>
    /// Blink the LED quickly.
    /// </summary>
    [Description("Fast Blink")]
    FastBlink,
    /// <summary>
    /// Blink the LED ultra quickly.
    /// </summary>
    [Description("Ultra Blink")]
    UltraBlink
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.API.VKB.Config;

namespace VKBLEDController.API.VKB.Enum;
public enum ColorMode
{
    /// <summary>
    /// Use <see cref="LedConfig.ColorOne"/>
    /// </summary>
    [Description("Color 1")]
    Color1 = 0,
    /// <summary>
    /// Use <see cref="LedConfig.ColorTwo"/>
    /// </summary>
    [Description("Color 2")]
    Color2,
    /// <summary>
    /// Alternate between <see cref="LedConfig.ColorOne"/> and <see cref="LedConfig.ColorTwo"/>.
    /// </summary>
    [Description("Color 1/2")]
    Color1d2,
    /// <summary>
    /// Alternate between <see cref="LedConfig.ColorTwo"/> and <see cref="LedConfig.ColorOne"/>.
    /// </summary>
    [Description("Color 2/1")]
    Color2d1,
    /// <summary>
    /// Do something with <see cref="LedConfig.ColorOne"/> and <see cref="LedConfig.ColorTwo"/> but idk what.
    /// </summary>
    [Description("Color 1+2")]
    Color1p2,
    /// <summary>
    /// It does something?
    /// </summary>
    [Description("Color 1+")]
    Color1p,
    /// <summary>
    /// It does something?
    /// </summary>
    [Description("Color 2+")]
    Color2p
}

﻿using HidSharp.Reports.Units;

using System.Text.Json.Serialization;

using VKBLEDController.API.VKB.Enum;

namespace VKBLEDController.API.VKB.Config;
public class LedConfig : IEquatable<LedConfig>
{
    /// <summary>
    /// The bitwise mask to set the ID bits in <see cref="_value"/>.
    /// </summary>
    private const uint ID_MASK          = 0b_1111_1111_0000_0000_0000_0000_0000_0000;
    /// <summary>
    /// The bitwise mask to set the color mode bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_MODE_MASK  = 0b_0000_0000_1110_0000_0000_0000_0000_0000;
    /// <summary>
    /// The bitwise mask to set the led mode bits in <see cref="_value"/>.
    /// </summary>
    private const uint LED_MODE_MASK    = 0b_0000_0000_0001_1100_0000_0000_0000_0000;
    // BGR color order.
    /// <summary>
    /// The bitwise mask to set the blue part of the color two bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_TWO_BMASK  = 0b_0000_0000_0000_0011_1000_0000_0000_0000;
    /// <summary>
    /// The bitwise mask to set the green part of the color two bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_TWO_GMASK  = 0b_0000_0000_0000_0000_0111_0000_0000_0000;
    /// <summary>
    /// The bitwise mask to set the red part of the color two bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_TWO_RMASK  = 0b_0000_0000_0000_0000_0000_1110_0000_0000;
    // BGR color order.
    /// <summary>
    /// The bitwise mask to set the blue part of the color one bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_ONE_BMASK  = 0b_0000_0000_0000_0000_0000_0001_1100_0000;
    /// <summary>
    /// The bitwise mask to set the green part of the color one bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_ONE_GMASK  = 0b_0000_0000_0000_0000_0000_0000_0011_1000;
    /// <summary>
    /// The bitwise mask to set the red part of the color one bits in <see cref="_value"/>.
    /// </summary>
    private const uint COLOR_ONE_RMASK  = 0b_0000_0000_0000_0000_0000_0000_0000_0111;

    // Offsets for the mask values.
    /// <summary>
    /// The number of bits the ID needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int ID_OFFSET         = 24;
    /// <summary>
    /// The number of bits the color mode needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_MODE_OFFSET = 21;
    /// <summary>
    /// The number of bits the led mode needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int LED_MODE_OFFSET   = 18;

    /// <summary>
    /// The number of bits the blue part of color two needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_TWO_BOFFSET = 15;
    /// <summary>
    /// The number of bits the green part of color two needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_TWO_GOFFSET = 12;
    /// <summary>
    /// The number of bits the red part of color two needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_TWO_ROFFSET = 9;

    /// <summary>
    /// The number of bits the blue part of color one needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_ONE_BOFFSET = 6;
    /// <summary>
    /// The number of bits the green part of color one needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_ONE_GOFFSET = 3;
    /// <summary>
    /// The number of bits the red part of color one needs 
    /// to be shifted in a <see cref="uint"/> before it is applied to <see cref="_value"/>.
    /// </summary>
    private const int COLOR_ONE_ROFFSET = 0;

    /// <summary>
    /// The raw value of this four <see cref="byte"/> config value.
    /// </summary>
    private uint _value;

    /// <summary>
    /// Creates a new config value for modifying.
    /// </summary>
    public LedConfig() { }

    /// <summary>
    /// Populates the config value with raw data.
    /// </summary>
    /// <param name="raw">The four byte long config data.</param>
    public LedConfig(byte[] raw)
    {
        // Take the ID from the first byte ...
        _value = raw[0];

        // ... and the remaining three bytes are in big endian order,
        // so we grab and add them to the _value in reverse order so they match
        // with C#s little endian order. 
        for (int i = raw.Length - 1; i >= 1; i--)
        {
            // shift by a byte and add the new data
            _value = (_value << 8) | raw[i];
        }
    }

    /// <summary>
    /// Creates a new instance with the configured
    /// settings.
    /// </summary>
    /// <param name="id">The LED ID.</param>
    /// <param name="colorMode">The <see cref="Enum.ColorMode"/> to use.</param>
    /// <param name="ledMode">The <see cref="Enum.LedMode"/></param>
    /// <param name="colorOne">The hex code for the first color.</param>
    /// <param name="colorTwo">The hex code for the second color.</param>
    [JsonConstructor]
    public LedConfig(int id, ColorMode colorMode,
        LedMode ledMode, string colorOne, string colorTwo)
    {
        Id = id;
        ColorMode = colorMode;
        LedMode = ledMode;
        ColorOne = colorOne;
        ColorTwo = colorTwo;
    }

    /// <summary>
    /// To be used with the value from <see cref="ToString"/>.
    /// </summary>
    /// <param name="raw">The value that was received as a string from <see cref="ToString"/>.</param>
    public LedConfig(uint raw)
    {
        _value = raw;
    }

    /// <summary>
    /// Converts <see cref="_value"/> to a string.
    /// </summary>
    /// <returns>A string representing this config.</returns>
    public override string ToString()
    {
        return Convert.ToString(_value, toBase: 2);
    }

    /// <summary>
    /// The LED ID on the VKB controller.
    /// </summary>
    public int Id
    {
        get => (int)GetValue(ID_MASK, ID_OFFSET);
        set => SetValue(ID_MASK, (uint)value, ID_OFFSET);
    }

    /// <summary>
    /// The <see cref="ColorMode"/> of this LED.
    /// </summary>
    public ColorMode ColorMode
    {
        get => (ColorMode)GetValue(COLOR_MODE_MASK, COLOR_MODE_OFFSET);
        set => SetValue(COLOR_MODE_MASK, (uint)value, 21);
    }

    /// <summary>
    /// The <see cref="LedMode"/> of this LED.
    /// </summary>
    public LedMode LedMode
    {
        get => (LedMode)GetValue(LED_MODE_MASK, LED_MODE_OFFSET);
        set => SetValue(LED_MODE_MASK, (uint)value, LED_MODE_OFFSET);
    }

    /// <summary>
    /// The first color of this LED.
    /// </summary>
    public string ColorOne
    {
        get
        {
            uint b = GetValue(COLOR_ONE_BMASK, COLOR_ONE_BOFFSET);
            uint g = GetValue(COLOR_ONE_GMASK, COLOR_ONE_GOFFSET);
            uint r = GetValue(COLOR_ONE_RMASK, COLOR_ONE_ROFFSET);

            return GetHexString([r, g, b]);
        }

        set
        {
            var bits = GetHexBits(value);

            SetValue(COLOR_ONE_BMASK, bits[2], COLOR_ONE_BOFFSET);
            SetValue(COLOR_ONE_GMASK, bits[1], COLOR_ONE_GOFFSET);
            SetValue(COLOR_ONE_RMASK, bits[0], COLOR_ONE_ROFFSET);
        }
    }

    /// <summary>
    /// The second color of this LED.
    /// </summary>
    public string ColorTwo
    {
        get
        {
            uint b = GetValue(COLOR_TWO_BMASK, COLOR_TWO_BOFFSET);
            uint g = GetValue(COLOR_TWO_GMASK, COLOR_TWO_GOFFSET);
            uint r = GetValue(COLOR_TWO_RMASK, COLOR_TWO_ROFFSET);

            return GetHexString([r, g, b]);
        }

        set
        {
            var bits = GetHexBits(value);

            SetValue(COLOR_TWO_BMASK, bits[2], COLOR_TWO_BOFFSET);
            SetValue(COLOR_TWO_GMASK, bits[1], COLOR_TWO_GOFFSET);
            SetValue(COLOR_TWO_RMASK, bits[0], COLOR_TWO_ROFFSET);
        }
    }

    /// <summary>
    /// Gets the <see cref="byte[]"/> representation of this configuration.
    /// </summary>
    /// <remarks>
    /// The bytes of this array are in the order they need to be when sent to
    /// the <see cref="HidSharp.HidDevice"/>.
    /// </remarks>
    /// <returns>A <see cref="byte[]"/> with a length of 4.</returns>
    public byte[] GetBytes()
        => [
            // the ID is the first byte ...
            (byte)(_value >> 24),
            // ... and the remaining three bytes of data need to be in
            // big endian order, so we start at the leftmost bits.
            (byte)_value,
            (byte)(_value >> 8),
            (byte)(_value >> 16),
        ];

    /// <summary>
    /// Sets a part of <see cref="_value"/>.
    /// </summary>
    /// <param name="mask">The mask to use.</param>
    /// <param name="newValue">The new value.</param>
    /// <param name="offset">The offset applied to the new value.</param>
    private void SetValue(uint mask, uint newValue, int offset)
    {
        // set the mask ...
        uint temp = _value & ~mask;
        // ... offset the new value ...
        newValue <<= offset;
        // ... and apply the change.
        _value = temp | (newValue & mask);
    }

    /// <summary>
    /// Gets a part of <see cref="_value"/>.
    /// </summary>
    /// <param name="mask">The mask to use.</param>
    /// <param name="offset">The offset applied to the result to create a properly aligned <see cref="uint"/>.</param>
    /// <returns>A <see cref="uint"/> representing a part of <see cref="_value"/>.</returns>
    private uint GetValue(uint mask, int offset) 
        => (_value & mask) >> offset;

    /// <summary>
    /// Gets the bit representation of a hexadecimal color code for a VKB controller.
    /// </summary>
    /// <remarks>
    /// VKB limits the colors on their devices to simple RGB values that are between 0 and 7.
    /// Due to this, any hex code conversions will not be perfect.
    /// </remarks>
    /// <param name="hex">The hex color code.</param>
    /// <returns>A <see cref="uint[]"/> containing three <see cref="uint"/> color values.</returns>
    /// <exception cref="ArgumentException"></exception>
    public static uint[] GetHexBits(string hex)
    {
        // get the hex digits without the # sign ...
        hex = hex.ToLower().TrimStart('#');

        // ... and if this is not a hex value ...
        if (hex.Length != 3 && hex.Length != 6)
            // ... throw an error ...
            throw new ArgumentException($"An invalid hex code was provided.", nameof(hex));

        // ... otherwise, if this is a three digit hex ...
        if (hex.Length == 3)
        {
            // ... duplicate the values (FAB becomes FFAABB) ...
            hex = new string(hex.SelectMany<char, char>(c => [c, c]).ToArray());
        }

        // ... then split the hex into its three different parts ...
        string[] hexParts = [hex[..2], hex[2..4], hex[4..]];

        // ... and build the uint array ...
        uint[] res = hexParts.Select(e =>
        {
            // ... by converting the number from base 16 ...
            uint hexNum = Convert.ToUInt32(e, 16);
            // ... taking the smaller number ...
            uint hexMin = Math.Min(hexNum, 255);
            // ... then doing some math to make the value between 0 and 7 ...
            uint hexFinal = (uint)Math.Round(hexMin / 255.0 * 7);
            return hexFinal;
        }).ToArray();

        // ... finally, return the uint array.
        return res;
    }

    /// <summary>
    /// Converts an array of three <see cref="uint"/>s into its hexadecimal color code
    /// equivalent.
    /// </summary>
    /// <param name="bits">The three VKB color code values as <see cref="uint"/>s.</param>
    /// <returns>A hex color code.</returns>
    /// <exception cref="ArgumentException"></exception>
    private static string GetHexString(uint[] bits)
    {
        // if there are not three units in this array ...
        if (bits.Length != 3)
            // ... throw an error ...
            throw new ArgumentException($"The length of {nameof(bits)} must be 3.", nameof(bits));

        // ... otherwise, get the parts of the hex code ...
        IEnumerable<string> hexParts = bits.Select(e =>
        {
            // ... by taking the min between the VKB value and 7 ...
            uint hexMin = Math.Min(e, 7);
            // ... turn the 0-7 value into a 0-255 value ...
            uint hexNum = (uint)Math.Round(hexMin / 7.0 * 255);
            // ... and convert the number to a base 16 string, padding the left with 0s as needed ...
            string hexFinal = Convert.ToString(hexNum, toBase: 16).PadLeft(2, '0');
            return hexFinal;
        });
        
        // ... then returning the complete hex string.
        return '#' + string.Join(string.Empty, hexParts);
    }

    public bool Equals(LedConfig? other)
        => GetHashCode().Equals(other?.GetHashCode());

    public override bool Equals(object? obj)
        => Equals(obj as LedConfig);

    public override int GetHashCode()
        => _value.GetHashCode();
}

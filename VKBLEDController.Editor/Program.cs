using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using VKBLEDController.Editor.GUI;
using VKBLEDController.Editor.Hosting;

namespace VKBLEDController.Editor;

internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(params string[] args)
        => CreateBuilder(args)
            .Build()
            .RunForm<Startup, Main>();

    /// <summary>
    /// Build the host for the app.
    /// </summary>
    /// <param name="args">The program arguments.</param>
    /// <returns>An <see cref="IHostBuilder"/> for the app.</returns>
    private static IHostBuilder CreateBuilder(string[] args)
        => Host.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
}
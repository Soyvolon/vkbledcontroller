﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Editor.GUI;
using VKBLEDController.Editor.Hosting;
using VKBLEDController.Editor.Properties;

namespace VKBLEDController.Editor;
public class Startup(IConfiguration configuration) : IFormStartup
{
    public void ConfigureServices(IServiceCollection services)
    {

        // Register forms
        services.AddTransient<Main>();
    }

    public void Configure(IHost host)
    {
        if (Settings.Default.UpgradeSettings)
        {
            Settings.Default.Upgrade();
            Settings.Default.UpgradeSettings = false;
            Settings.Default.Save();
        }

        // To customize application configuration such as set high DPI settings or default font,
        // see https://aka.ms/applicationconfiguration.
        ApplicationConfiguration.Initialize();
    }
}

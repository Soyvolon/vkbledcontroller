﻿namespace VKBLEDController.Editor.GUI.Popups;

partial class InputStringDialogBox
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        submitButton = new Button();
        cancelButton = new Button();
        textInput = new TextBox();
        primaryLabel = new Label();
        SuspendLayout();
        // 
        // submitButton
        // 
        submitButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        submitButton.DialogResult = DialogResult.OK;
        submitButton.Location = new Point(483, 60);
        submitButton.Name = "submitButton";
        submitButton.Size = new Size(75, 23);
        submitButton.TabIndex = 0;
        submitButton.Text = "Submit";
        submitButton.UseVisualStyleBackColor = true;
        submitButton.Click += EndButton_Click;
        // 
        // cancelButton
        // 
        cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        cancelButton.DialogResult = DialogResult.Cancel;
        cancelButton.Location = new Point(402, 60);
        cancelButton.Name = "cancelButton";
        cancelButton.Size = new Size(75, 23);
        cancelButton.TabIndex = 1;
        cancelButton.Text = "Cancel";
        cancelButton.UseVisualStyleBackColor = true;
        cancelButton.Click += EndButton_Click;
        // 
        // textInput
        // 
        textInput.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        textInput.Location = new Point(12, 31);
        textInput.Name = "textInput";
        textInput.Size = new Size(546, 23);
        textInput.TabIndex = 2;
        // 
        // primaryLabel
        // 
        primaryLabel.AutoSize = true;
        primaryLabel.Location = new Point(12, 13);
        primaryLabel.Name = "primaryLabel";
        primaryLabel.Size = new Size(38, 15);
        primaryLabel.TabIndex = 3;
        primaryLabel.Text = "label1";
        // 
        // InputStringDialogBox
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(570, 95);
        Controls.Add(primaryLabel);
        Controls.Add(textInput);
        Controls.Add(cancelButton);
        Controls.Add(submitButton);
        Name = "InputStringDialogBox";
        Text = "InputStringDialogBox";
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Button submitButton;
    private Button cancelButton;
    private Label primaryLabel;
    public TextBox textInput;
}
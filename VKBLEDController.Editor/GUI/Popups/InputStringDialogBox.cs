﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VKBLEDController.Editor.GUI.Popups;
public partial class InputStringDialogBox : Form
{
    public InputStringDialogBox(string header, string label)
    {
        InitializeComponent();

        Text = header;
        primaryLabel.Text = label;
    }

    public void EndButton_Click(object sender, EventArgs e)
        => Close();
}

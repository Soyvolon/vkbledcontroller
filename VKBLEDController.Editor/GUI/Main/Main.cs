﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Editor.GUI.SettingsEdit;
using VKBLEDController.Editor.Properties;

namespace VKBLEDController.Editor.GUI;
public partial class Main : Form
{
    private const string LAST_SAVE_FORMAT = "[Last Save: {0:g}]";
    private const string SAVE_COMPLETE_MSG = "[Save Complete!]";

    private string InstanceDataPath 
    { 
        get => Environment.ExpandEnvironmentVariables(Settings.Default.InstanceDataPath); 
    }

    public Main()
    {
        InitializeComponent();
        Text = "VKB LED Controller Config Editor " + Settings.Default.AppVersion;
    }

    public async void Main_Load(object sender, EventArgs e)
    {
        // Do initial loading...
        await UpdateInstancesAsync();
    }

    private void SettingsMenu_Click(object sender, EventArgs e)
    {
        using var settings = new SettingsForm();
        settings.ShowDialog(this);
    }
}

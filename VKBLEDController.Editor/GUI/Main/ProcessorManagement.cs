﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Module;
using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Util;
using VKBLEDController.Editor.Elements;

using VKBLEDController.GUI.Config.Editors;

namespace VKBLEDController.Editor.GUI;
public partial class Main
{

    #region Processors
    private List<(Type type, DataProcessorAttribute attr)> AvailableDataProcessors { get; set; } = [];
    private List<Type> ActiveProcessorTypes { get; set; } = [];
    private (Type type, DataProcessorAttribute attr)? ActiveProcessor { get; set; }
    private IDataProcessor? ActiveProcessorData { get; set; }

    private async Task BuildActiveProcessors()
    {
        if (ActiveModule is null)
            return;

        await ActiveModule.LoadProcessorsAsync();

        ActiveProcessorTypes = ActiveModule.DataProcessors
            .Select(e => e.GetType())
            .ToList();
    }

    private void ReloadProcessors()
    {
        AvailableDataProcessors = AssetLists.GetDataProcessors();

        processorFlow.Invoke(() =>
        {
            processorFlow.Controls.Clear();

            foreach (var (type, attr) in AvailableDataProcessors)
            {
                bool check = ActiveProcessorTypes.Contains(type);

                processorFlow.Controls.Add(new ActivatableControl(attr.Name, check,
                    ProcessorEnabledChangedAsync, ProcessorEditRequested));
            }
        });
    }

    private async Task<bool> ProcessorEnabledChangedAsync(string name, bool newEnabledStatus)
    {
        var (type, attr) = AvailableDataProcessors
            .Where(e => e.attr.Name == name)
            .FirstOrDefault();

        if (type is null || attr is null)
            return false;

        if (newEnabledStatus)
            return await TryEnableProcessorAsync(type, attr);
        return TryDisableProcessorAsync(type, attr);
    }

    private async Task<bool> TryEnableProcessorAsync(Type type, DataProcessorAttribute attr)
    {
        if (ActiveModule is null)
            return false;

        return await ActiveModule.AddProcessorAsync(type, attr);
    }

    private bool TryDisableProcessorAsync(Type type, DataProcessorAttribute attr)
    {
        if (ActiveModule is null)
            return false;

        return ActiveModule.RemoveProcessor(type, attr);
    }

    private void ProcessorEditRequested(string name)
    {
        var (type, attr) = AvailableDataProcessors
            .Where(e => e.attr.Name == name)
            .FirstOrDefault();

        if (type is null || attr is null || ActiveModule is null)
            return;

        if (ActiveProcessor is not null)
        {
            var res = MessageBox.Show($"The Processor: {ActiveProcessor.Value.attr.Name} is currently being edited. Editing a new Processor" +
                $" will result in all unsaved changes being lost.\n\nContinue to edit the new Processor?",
                "Edit Processor", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res != DialogResult.Yes)
                return;
        }

        ActiveProcessor = (type, attr);
        ActiveProcessorData = ActiveModule.DataProcessors
            .Where(e => e.GetType().AssemblyQualifiedName == type.AssemblyQualifiedName)
            .FirstOrDefault();

        if (ActiveProcessor is not null && ActiveProcessorData is null
            || ActiveProcessor is null && ActiveProcessorData is not null)
        {
            ActiveProcessor = null;
            ActiveProcessorData = null;
        }

        processorSettingsBox.Enabled = ActiveProcessorData is not null;
        ReloadProcessorSettingsDropdown();
        ReloadProcessorBindingFlow();
    }
    #endregion

    #region Bindings
    private List<Type> ActiveBinds { get; set; } = [];

    private void ReloadProcessorBindingFlow()
    {
        if (ActiveProcessorData is ProcessorBase baseProcessor)
        {
            ActiveBinds = AvailableDataCollectors
            .Where(e => baseProcessor.Settings.ListenToCollectors.Contains(e.attr.Name))
            .Select(e => e.type)
            .ToList();
        }

        collectorBindFlow.Invoke(() =>
        {
            collectorBindFlow.Controls.Clear();

            foreach (var activeCollectors in ActiveCollectorTypes)
            {
                var index = AvailableDataCollectors.FindIndex(e => e.type == activeCollectors);
                if (index == -1)
                    continue;

                var (type, attr) = AvailableDataCollectors[index];
                var isActive = ActiveBinds.Contains(type);

                collectorBindFlow.Controls.Add(new DataProcessorBindingControl(
                    attr.Name, isActive, OnBindToggleRequestAsync));
            }
        });
    }

    private Task<bool> OnBindToggleRequestAsync(string name, bool newState)
    {
        var (type, attr) = AvailableDataCollectors
            .Where(e => e.attr.Name == name)
            .FirstOrDefault();

        if (type is null || attr is null)
            return Task.FromResult(false);

        if (ActiveProcessorData is not ProcessorBase baseProcessor)
            return Task.FromResult(false);

        if (newState)
        {
            baseProcessor.Settings.ListenToCollectors.Add(name);
            ActiveBinds.Add(type);
        }
        else
        {
            _ = baseProcessor.Settings.ListenToCollectors.Remove(name);
            ActiveBinds.Remove(type);
        }

        return Task.FromResult(true);
    }
    #endregion

    #region Processor Settings
    private List<(PropertyInfo prop, ModuleSettingsAttribute attr)> ProcessorSettingPropertyPairs { get; set; } = [];
    private (PropertyInfo prop, ModuleSettingsAttribute attr)? ActiveProcessorSettingPropertyPair { get; set; }

    private void ReloadProcessorSettingsDropdown()
    {
        if (ActiveProcessorData is null)
        {
            ProcessorSettingPropertyPairs.Clear();
            return;
        }

        ProcessorSettingPropertyPairs = ModuleSettings.GetSettingPropertyPairs(ActiveProcessorData)
            .ToList();

        processorSettingsDropdown.Invoke(() =>
        {
            processorSettingsDropdown.Items.Clear();
            foreach (var (prop, attr) in ProcessorSettingPropertyPairs)
                processorSettingsDropdown.Items.Add(attr.Name);

            processorSettingsDropdown.SelectedIndex
                = processorSettingsDropdown.Items.IndexOf(ActiveProcessorSettingPropertyPair?.attr.Name);
        });
    }
    #endregion

    #region Events
    private void EditProcessorSetting_Click(object sender, EventArgs e)
    {
        if (ActiveProcessor is null)
            return;

        if (processorSettingsDropdown.SelectedIndex == -1)
            return;

        ActiveProcessorSettingPropertyPair = ProcessorSettingPropertyPairs[processorSettingsDropdown.SelectedIndex];
        var settingsType = ActiveProcessorSettingPropertyPair.Value.prop.PropertyType;

        if (!EditorHelper.Editors.TryGetValue(settingsType, out var editorType))
        {
            MessageBox.Show("The selected settings do not have a GUI registered to edit with." +
                " If you wish to edit these settings, please modify the JSON file directly.",
                "No GUI Editor Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (Activator.CreateInstance(editorType) is not IEditorGUIBase gui)
        {
            MessageBox.Show("The selected settings have a registered type for editing, but" +
                "it is not a renderable control. If you wish to edit these settings, please " +
                "modify the JSON file directly.",
                "GUI Loading Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var settings = ActiveProcessorSettingPropertyPair.Value.prop.GetValue(ActiveProcessorData);
        if (settings is not null && ActiveProcessorData is not null)
            gui.SetSettingsInstance(settings, ActiveProcessorData);

        processorSettingEditor.Controls.Clear();
        processorSettingEditor.Controls.Add(gui as UserControl);
    }
    #endregion
}

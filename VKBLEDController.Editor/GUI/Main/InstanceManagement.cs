﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Instance;
using VKBLEDController.Config.Util;
using VKBLEDController.Editor.GUI.Popups;
using VKBLEDController.Editor.Properties;

namespace VKBLEDController.Editor.GUI;
public partial class Main
{
    private List<InstanceSettings> Instances { get; set; } = [];
    private InstanceSettings? ActiveInstance { get; set; }

    private async Task UpdateInstancesAsync()
    {
        Instances = await AssetLists.GetInstancesAsync(InstanceDataPath);

        instanceDropdown.Invoke(() =>
        {
            instanceDropdown.Items.Clear();
            foreach (var instance in Instances)
                instanceDropdown.Items.Add(instance.Name);
            instanceDropdown.SelectedIndex = instanceDropdown.Items.IndexOf(ActiveInstance?.Name);
        });
    }

    private async Task SetActiveInstanceAsync(InstanceSettings? instance)
    {
        ActiveInstance = instance;

        moduleBox.Invoke(() =>
        {
            moduleBox.Enabled = instance is not null;
            activeInstanceLabel.Text = $"Active Instance: {instance?.Name ?? "N/A"}";
            lastInstanceSaveLabel.Text = string.Format(LAST_SAVE_FORMAT, instance?.LastSave);
        });

        if (instance is null)
            return;

        await UpdateModulesAsync();
    }

    #region Events
    public async void CreateInstance_Click(object sender, EventArgs e)
    {
        using var popup = new InputStringDialogBox("Instance Name", "Input New Instance Name:");
        var res = popup.ShowDialog(this);

        if (res != DialogResult.OK)
            return;

        var instance = new InstanceSettings()
        {
            Name = popup.textInput.Text,
            _rootPath = InstanceDataPath
        };

        await instance.SaveAsync();
        await UpdateInstancesAsync();
    }

    public async void LoadInstance_Click(object sender, EventArgs e)
    {
        if (instanceDropdown.SelectedIndex == -1)
            return;

        if (ActiveInstance is not null)
        {
            var res = MessageBox.Show($"The instance: {ActiveInstance.Name} is currently loaded. Loading a new instance" +
                $" will result in all unsaved changes being lost.\n\nContinue to load new instance?",
                "Load Instance", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res != DialogResult.Yes)
                return;
        }

        var instance = Instances[instanceDropdown.SelectedIndex];

        await SetActiveInstanceAsync(instance);
        await UpdateInstancesAsync();
    }

    public async void SaveInstance_Click(object sender, EventArgs e)
    {
        if (ActiveInstance is null)
            return;

        await ActiveInstance.SaveAsync();

        Invoke(() =>
        {
            lastInstanceSaveLabel.Text = SAVE_COMPLETE_MSG;
            if (ActiveModule is not null)
                lastModuleSaveLabel.Text = SAVE_COMPLETE_MSG;

            _ = Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(2));

                lastInstanceSaveLabel.Text = string.Format(LAST_SAVE_FORMAT, ActiveInstance?.LastSave);
                if (ActiveModule is not null)
                    lastModuleSaveLabel.Text = string.Format(LAST_SAVE_FORMAT, ActiveModule?.LastSave);
            });
        });
    }

    public async void DeleteInstance_Click(object sender, EventArgs e)
    {
        if (instanceDropdown.SelectedIndex == -1)
            return;

        var instance = Instances[instanceDropdown.SelectedIndex];

        var res = MessageBox.Show($"Are you sure you want to delete the instance: {instance.Name}?",
            "Delete Instance", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        if (res != DialogResult.Yes)
            return;

        if (instance.Name.Equals(ActiveInstance?.Name))
            await SetActiveInstanceAsync(null);

        instance.Delete();

        await UpdateInstancesAsync();
    }
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Module;
using VKBLEDController.Config.Util;
using VKBLEDController.Editor.Elements;
using VKBLEDController.GUI.Config.Editors;

namespace VKBLEDController.Editor.GUI;
public partial class Main
{
    #region Collectors
    private List<(Type type, DataCollectorAttribute attr)> AvailableDataCollectors { get; set; } = [];
    private List<Type> ActiveCollectorTypes { get; set; } = [];
    private (Type type, DataCollectorAttribute attr)? ActiveCollector { get; set; }
    private IDataCollector? ActiveCollectorData { get; set; }

    private async Task BuildActiveCollectors()
    {
        if (ActiveModule is null)
            return;

        await ActiveModule.LoadCollectorsAsync();

        ActiveCollectorTypes = ActiveModule.DataCollectors
            .Select(e => e.GetType())
            .ToList();
    }

    private void ReloadCollectors()
    {
        AvailableDataCollectors = AssetLists.GetDataCollectors();

        collectorsFlow.Invoke(() =>
        {
            collectorsFlow.Controls.Clear();

            foreach (var (type, attr) in AvailableDataCollectors)
            {
                bool check = ActiveCollectorTypes.Contains(type);

                collectorsFlow.Controls.Add(new ActivatableControl(attr.Name, check, 
                    CollectorEnabledChangedAsync, CollectorEditRequested));
            }
        });
    }

    private async Task<bool> CollectorEnabledChangedAsync(string name, bool newEnabledStatus)
    {
        var (type, attr) = AvailableDataCollectors
            .Where(e => e.attr.Name == name)
            .FirstOrDefault();

        if (type is null || attr is null)
            return false;

        if (newEnabledStatus)
            return await TryEnableCollectorAsync(type, attr);
        return TryDisableCollectorAsync(type, attr);
    }

    private async Task<bool> TryEnableCollectorAsync(Type type, DataCollectorAttribute attr)
    {
        if (ActiveModule is null)
            return false;

        return await ActiveModule.AddCollectorAsync(type, attr);
    }

    private bool TryDisableCollectorAsync(Type type, DataCollectorAttribute attr)
    {
        if (ActiveModule is null)
            return false;

        return ActiveModule.RemoveCollector(type, attr);
    }

    private void CollectorEditRequested(string name)
    {
        var (type, attr) = AvailableDataCollectors
            .Where(e => e.attr.Name == name)
            .FirstOrDefault();

        if (type is null || attr is null || ActiveModule is null)
            return;

        if (ActiveCollector is not null)
        {
            var res = MessageBox.Show($"The collector: {ActiveCollector.Value.attr.Name} is currently being edited. Editing a new collector" +
                $" will result in all unsaved changes being lost.\n\nContinue to edit the new collector?",
                "Edit Collector", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res != DialogResult.Yes)
                return;
        }

        ActiveCollector = (type, attr);
        ActiveCollectorData = ActiveModule.DataCollectors
            .Where(e => e.GetType().AssemblyQualifiedName == type.AssemblyQualifiedName)
            .FirstOrDefault();

        if (ActiveCollector is not null && ActiveCollectorData is null
            || ActiveCollector is null && ActiveCollectorData is not null)
        {
            ActiveCollector = null;
            ActiveCollectorData = null;
        }

        collectorSettingsBox.Enabled = ActiveCollectorData is not null;
        ReloadCollectorSettingsDropdown();
    }
    #endregion

    #region Collector Settings
    private List<(PropertyInfo prop, ModuleSettingsAttribute attr)> CollectorSettingPropertyPairs { get; set; } = [];
    private (PropertyInfo prop, ModuleSettingsAttribute attr)? ActiveCollectorSettingPropertyPair { get; set; }

    private void ReloadCollectorSettingsDropdown()
    {
        if (ActiveCollectorData is null)
        {
            CollectorSettingPropertyPairs.Clear();
            return;
        }

        CollectorSettingPropertyPairs = ModuleSettings.GetSettingPropertyPairs(ActiveCollectorData)
            .ToList();

        collectorSettingsDropdown.Invoke(() =>
        {
            collectorSettingsDropdown.Items.Clear();
            foreach (var (prop, attr) in CollectorSettingPropertyPairs)
                collectorSettingsDropdown.Items.Add(attr.Name);

            collectorSettingsDropdown.SelectedIndex 
                = collectorSettingsDropdown.Items.IndexOf(ActiveCollectorSettingPropertyPair?.attr.Name);
        });
    }
    #endregion

    #region Events
    private void EditCollectorSetting_Click(object sender, EventArgs e)
    {
        if (ActiveCollector is null)
            return;

        if (collectorSettingsDropdown.SelectedIndex == -1)
            return;

        ActiveCollectorSettingPropertyPair = CollectorSettingPropertyPairs[collectorSettingsDropdown.SelectedIndex];
        var settingsType = ActiveCollectorSettingPropertyPair.Value.prop.PropertyType;

        if (!EditorHelper.Editors.TryGetValue(settingsType, out var editorType))
        {
            MessageBox.Show("The selected settings do not have a GUI registered to edit with." +
                " If you wish to edit these settings, please modify the JSON file directly.",
                "No GUI Editor Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (Activator.CreateInstance(editorType) is not IEditorGUIBase gui)
        {
            MessageBox.Show("The selected settings have a registered type for editing, but" +
                "it is not a renderable control. If you wish to edit these settings, please " +
                "modify the JSON file directly.",
                "GUI Loading Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var settings = ActiveCollectorSettingPropertyPair.Value.prop.GetValue(ActiveCollectorData);
        if (settings is not null && ActiveCollectorData is not null)
            gui.SetSettingsInstance(settings, ActiveCollectorData);

        collectorSettingEditor.Controls.Clear();
        collectorSettingEditor.Controls.Add(gui as UserControl);
    }
    #endregion
}

﻿namespace VKBLEDController.Editor.GUI;

partial class Main
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        components = new System.ComponentModel.Container();
        moduleStatusStrip = new StatusStrip();
        activeModuleLabel = new ToolStripStatusLabel();
        lastModuleSaveLabel = new ToolStripStatusLabel();
        instanceDropdown = new ComboBox();
        createInstance = new Button();
        loadInstance = new Button();
        saveInstance = new Button();
        deleteInstance = new Button();
        groupBox1 = new GroupBox();
        moduleBox = new GroupBox();
        moduleDropdown = new ComboBox();
        deleteModule = new Button();
        loadModule = new Button();
        createModule = new Button();
        saveModule = new Button();
        moduleEditTabs = new TabControl();
        moduleTab = new TabPage();
        groupBox2 = new GroupBox();
        closeAfterModuleLaunch = new CheckBox();
        launchModule = new Button();
        label2 = new Label();
        minDeviceUpdateTime = new NumericUpDown();
        collectorTab = new TabPage();
        collectorSettingsBox = new GroupBox();
        editCollectorSetting = new Button();
        collectorSettingsDropdown = new ComboBox();
        collectorSettingEditor = new Panel();
        collectorsFlow = new FlowLayoutPanel();
        processorTab = new TabPage();
        processorSettingsBox = new GroupBox();
        editProcessorSettings = new Button();
        processorSettingsDropdown = new ComboBox();
        processorSettingEditor = new Panel();
        collectorBindFlow = new FlowLayoutPanel();
        processorFlow = new FlowLayoutPanel();
        label1 = new Label();
        ledTab = new TabPage();
        refreshLeds = new Button();
        addLedButton = new Button();
        ledFlow = new FlowLayoutPanel();
        onActionTooltip = new ToolTip(components);
        instanceStatusStrip = new StatusStrip();
        activeInstanceLabel = new ToolStripStatusLabel();
        lastInstanceSaveLabel = new ToolStripStatusLabel();
        menuStrip1 = new MenuStrip();
        settingsMenu = new ToolStripMenuItem();
        moduleStatusStrip.SuspendLayout();
        groupBox1.SuspendLayout();
        moduleBox.SuspendLayout();
        moduleEditTabs.SuspendLayout();
        moduleTab.SuspendLayout();
        groupBox2.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)minDeviceUpdateTime).BeginInit();
        collectorTab.SuspendLayout();
        collectorSettingsBox.SuspendLayout();
        processorTab.SuspendLayout();
        processorSettingsBox.SuspendLayout();
        ledTab.SuspendLayout();
        instanceStatusStrip.SuspendLayout();
        menuStrip1.SuspendLayout();
        SuspendLayout();
        // 
        // moduleStatusStrip
        // 
        moduleStatusStrip.Items.AddRange(new ToolStripItem[] { activeModuleLabel, lastModuleSaveLabel });
        moduleStatusStrip.Location = new Point(0, 511);
        moduleStatusStrip.Name = "moduleStatusStrip";
        moduleStatusStrip.Size = new Size(773, 22);
        moduleStatusStrip.TabIndex = 1;
        moduleStatusStrip.Text = "Active Instance: N/A";
        // 
        // activeModuleLabel
        // 
        activeModuleLabel.Name = "activeModuleLabel";
        activeModuleLabel.Size = new Size(112, 17);
        activeModuleLabel.Text = "Active Module: N/A";
        // 
        // lastModuleSaveLabel
        // 
        lastModuleSaveLabel.Name = "lastModuleSaveLabel";
        lastModuleSaveLabel.Size = new Size(69, 17);
        lastModuleSaveLabel.Text = "[Last Save: ]";
        // 
        // instanceDropdown
        // 
        instanceDropdown.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        instanceDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
        instanceDropdown.FormattingEnabled = true;
        instanceDropdown.Location = new Point(6, 22);
        instanceDropdown.Name = "instanceDropdown";
        instanceDropdown.Size = new Size(360, 23);
        instanceDropdown.TabIndex = 2;
        // 
        // createInstance
        // 
        createInstance.Location = new Point(6, 51);
        createInstance.Name = "createInstance";
        createInstance.Size = new Size(75, 23);
        createInstance.TabIndex = 3;
        createInstance.Text = "Create";
        createInstance.UseVisualStyleBackColor = true;
        createInstance.Click += CreateInstance_Click;
        // 
        // loadInstance
        // 
        loadInstance.Location = new Point(168, 51);
        loadInstance.Name = "loadInstance";
        loadInstance.Size = new Size(75, 23);
        loadInstance.TabIndex = 4;
        loadInstance.Text = "Load";
        loadInstance.UseVisualStyleBackColor = true;
        loadInstance.Click += LoadInstance_Click;
        // 
        // saveInstance
        // 
        saveInstance.Location = new Point(87, 51);
        saveInstance.Name = "saveInstance";
        saveInstance.Size = new Size(75, 23);
        saveInstance.TabIndex = 5;
        saveInstance.Text = "Save";
        saveInstance.UseVisualStyleBackColor = true;
        saveInstance.Click += SaveInstance_Click;
        // 
        // deleteInstance
        // 
        deleteInstance.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        deleteInstance.Location = new Point(291, 51);
        deleteInstance.Name = "deleteInstance";
        deleteInstance.Size = new Size(75, 23);
        deleteInstance.TabIndex = 6;
        deleteInstance.Text = "Delete";
        deleteInstance.UseVisualStyleBackColor = true;
        deleteInstance.Click += DeleteInstance_Click;
        // 
        // groupBox1
        // 
        groupBox1.Controls.Add(instanceDropdown);
        groupBox1.Controls.Add(deleteInstance);
        groupBox1.Controls.Add(loadInstance);
        groupBox1.Controls.Add(createInstance);
        groupBox1.Controls.Add(saveInstance);
        groupBox1.Location = new Point(12, 27);
        groupBox1.Name = "groupBox1";
        groupBox1.Size = new Size(372, 85);
        groupBox1.TabIndex = 7;
        groupBox1.TabStop = false;
        groupBox1.Text = "Instance Manager";
        // 
        // moduleBox
        // 
        moduleBox.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        moduleBox.Controls.Add(moduleDropdown);
        moduleBox.Controls.Add(deleteModule);
        moduleBox.Controls.Add(loadModule);
        moduleBox.Controls.Add(createModule);
        moduleBox.Controls.Add(saveModule);
        moduleBox.Enabled = false;
        moduleBox.Location = new Point(390, 27);
        moduleBox.Name = "moduleBox";
        moduleBox.Size = new Size(372, 85);
        moduleBox.TabIndex = 8;
        moduleBox.TabStop = false;
        moduleBox.Text = "Module Manager";
        // 
        // moduleDropdown
        // 
        moduleDropdown.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        moduleDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
        moduleDropdown.FormattingEnabled = true;
        moduleDropdown.Location = new Point(6, 22);
        moduleDropdown.Name = "moduleDropdown";
        moduleDropdown.Size = new Size(360, 23);
        moduleDropdown.TabIndex = 2;
        // 
        // deleteModule
        // 
        deleteModule.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        deleteModule.Location = new Point(291, 51);
        deleteModule.Name = "deleteModule";
        deleteModule.Size = new Size(75, 23);
        deleteModule.TabIndex = 6;
        deleteModule.Text = "Delete";
        deleteModule.UseVisualStyleBackColor = true;
        deleteModule.Click += DeleteModule_Click;
        // 
        // loadModule
        // 
        loadModule.Location = new Point(168, 51);
        loadModule.Name = "loadModule";
        loadModule.Size = new Size(75, 23);
        loadModule.TabIndex = 4;
        loadModule.Text = "Load";
        loadModule.UseVisualStyleBackColor = true;
        loadModule.Click += LoadModule_Click;
        // 
        // createModule
        // 
        createModule.Location = new Point(6, 51);
        createModule.Name = "createModule";
        createModule.Size = new Size(75, 23);
        createModule.TabIndex = 3;
        createModule.Text = "Create";
        createModule.UseVisualStyleBackColor = true;
        createModule.Click += CreateModule_Click;
        // 
        // saveModule
        // 
        saveModule.Location = new Point(87, 51);
        saveModule.Name = "saveModule";
        saveModule.Size = new Size(75, 23);
        saveModule.TabIndex = 5;
        saveModule.Text = "Save";
        saveModule.UseVisualStyleBackColor = true;
        saveModule.Click += SaveModule_Click;
        // 
        // moduleEditTabs
        // 
        moduleEditTabs.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        moduleEditTabs.Controls.Add(moduleTab);
        moduleEditTabs.Controls.Add(collectorTab);
        moduleEditTabs.Controls.Add(processorTab);
        moduleEditTabs.Controls.Add(ledTab);
        moduleEditTabs.Enabled = false;
        moduleEditTabs.Location = new Point(12, 112);
        moduleEditTabs.Name = "moduleEditTabs";
        moduleEditTabs.SelectedIndex = 0;
        moduleEditTabs.Size = new Size(750, 374);
        moduleEditTabs.TabIndex = 9;
        // 
        // moduleTab
        // 
        moduleTab.Controls.Add(groupBox2);
        moduleTab.Controls.Add(label2);
        moduleTab.Controls.Add(minDeviceUpdateTime);
        moduleTab.Location = new Point(4, 24);
        moduleTab.Name = "moduleTab";
        moduleTab.Size = new Size(742, 346);
        moduleTab.TabIndex = 3;
        moduleTab.Text = "Module";
        moduleTab.UseVisualStyleBackColor = true;
        // 
        // groupBox2
        // 
        groupBox2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        groupBox2.Controls.Add(closeAfterModuleLaunch);
        groupBox2.Controls.Add(launchModule);
        groupBox2.Location = new Point(3, 243);
        groupBox2.Name = "groupBox2";
        groupBox2.Size = new Size(736, 100);
        groupBox2.TabIndex = 4;
        groupBox2.TabStop = false;
        groupBox2.Text = "Module Running";
        // 
        // closeAfterModuleLaunch
        // 
        closeAfterModuleLaunch.AutoSize = true;
        closeAfterModuleLaunch.Location = new Point(9, 22);
        closeAfterModuleLaunch.Name = "closeAfterModuleLaunch";
        closeAfterModuleLaunch.Size = new Size(126, 19);
        closeAfterModuleLaunch.TabIndex = 3;
        closeAfterModuleLaunch.Text = "Close After Launch";
        closeAfterModuleLaunch.UseVisualStyleBackColor = true;
        // 
        // launchModule
        // 
        launchModule.Location = new Point(9, 47);
        launchModule.Name = "launchModule";
        launchModule.Size = new Size(165, 23);
        launchModule.TabIndex = 2;
        launchModule.Text = "Launch Module (Will Save)";
        launchModule.UseVisualStyleBackColor = true;
        launchModule.Click += LaunchModule_Click;
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new Point(12, 11);
        label2.Name = "label2";
        label2.Size = new Size(136, 15);
        label2.TabIndex = 1;
        label2.Text = "Min Device Update Time";
        // 
        // minDeviceUpdateTime
        // 
        minDeviceUpdateTime.Location = new Point(12, 29);
        minDeviceUpdateTime.Name = "minDeviceUpdateTime";
        minDeviceUpdateTime.Size = new Size(136, 23);
        minDeviceUpdateTime.TabIndex = 0;
        minDeviceUpdateTime.ValueChanged += MinDeviceUpdateTime_ValueChanged;
        // 
        // collectorTab
        // 
        collectorTab.Controls.Add(collectorSettingsBox);
        collectorTab.Controls.Add(collectorsFlow);
        collectorTab.Location = new Point(4, 24);
        collectorTab.Name = "collectorTab";
        collectorTab.Padding = new Padding(3);
        collectorTab.Size = new Size(742, 346);
        collectorTab.TabIndex = 0;
        collectorTab.Text = "Collectors";
        collectorTab.UseVisualStyleBackColor = true;
        // 
        // collectorSettingsBox
        // 
        collectorSettingsBox.Controls.Add(editCollectorSetting);
        collectorSettingsBox.Controls.Add(collectorSettingsDropdown);
        collectorSettingsBox.Controls.Add(collectorSettingEditor);
        collectorSettingsBox.Enabled = false;
        collectorSettingsBox.Location = new Point(236, 6);
        collectorSettingsBox.Name = "collectorSettingsBox";
        collectorSettingsBox.Size = new Size(500, 339);
        collectorSettingsBox.TabIndex = 10;
        collectorSettingsBox.TabStop = false;
        collectorSettingsBox.Text = "Collector Settings";
        // 
        // editCollectorSetting
        // 
        editCollectorSetting.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        editCollectorSetting.Location = new Point(424, 29);
        editCollectorSetting.Name = "editCollectorSetting";
        editCollectorSetting.Size = new Size(70, 23);
        editCollectorSetting.TabIndex = 13;
        editCollectorSetting.Text = "Edit";
        editCollectorSetting.UseVisualStyleBackColor = true;
        editCollectorSetting.Click += EditCollectorSetting_Click;
        // 
        // collectorSettingsDropdown
        // 
        collectorSettingsDropdown.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        collectorSettingsDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
        collectorSettingsDropdown.FormattingEnabled = true;
        collectorSettingsDropdown.Location = new Point(6, 29);
        collectorSettingsDropdown.Name = "collectorSettingsDropdown";
        collectorSettingsDropdown.Size = new Size(412, 23);
        collectorSettingsDropdown.TabIndex = 10;
        // 
        // collectorSettingEditor
        // 
        collectorSettingEditor.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
        collectorSettingEditor.AutoScroll = true;
        collectorSettingEditor.Location = new Point(6, 58);
        collectorSettingEditor.Name = "collectorSettingEditor";
        collectorSettingEditor.Size = new Size(488, 273);
        collectorSettingEditor.TabIndex = 9;
        // 
        // collectorsFlow
        // 
        collectorsFlow.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
        collectorsFlow.AutoScroll = true;
        collectorsFlow.Location = new Point(6, 6);
        collectorsFlow.Name = "collectorsFlow";
        collectorsFlow.Size = new Size(224, 331);
        collectorsFlow.TabIndex = 9;
        // 
        // processorTab
        // 
        processorTab.Controls.Add(processorSettingsBox);
        processorTab.Controls.Add(collectorBindFlow);
        processorTab.Controls.Add(processorFlow);
        processorTab.Controls.Add(label1);
        processorTab.Location = new Point(4, 24);
        processorTab.Name = "processorTab";
        processorTab.Padding = new Padding(3);
        processorTab.Size = new Size(742, 346);
        processorTab.TabIndex = 1;
        processorTab.Text = "Processors";
        processorTab.UseVisualStyleBackColor = true;
        // 
        // processorSettingsBox
        // 
        processorSettingsBox.Controls.Add(editProcessorSettings);
        processorSettingsBox.Controls.Add(processorSettingsDropdown);
        processorSettingsBox.Controls.Add(processorSettingEditor);
        processorSettingsBox.Enabled = false;
        processorSettingsBox.Location = new Point(233, 8);
        processorSettingsBox.Name = "processorSettingsBox";
        processorSettingsBox.Size = new Size(500, 339);
        processorSettingsBox.TabIndex = 16;
        processorSettingsBox.TabStop = false;
        processorSettingsBox.Text = "Processor Settings";
        // 
        // editProcessorSettings
        // 
        editProcessorSettings.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        editProcessorSettings.Location = new Point(424, 29);
        editProcessorSettings.Name = "editProcessorSettings";
        editProcessorSettings.Size = new Size(70, 23);
        editProcessorSettings.TabIndex = 13;
        editProcessorSettings.Text = "Edit";
        editProcessorSettings.UseVisualStyleBackColor = true;
        editProcessorSettings.Click += EditProcessorSetting_Click;
        // 
        // processorSettingsDropdown
        // 
        processorSettingsDropdown.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        processorSettingsDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
        processorSettingsDropdown.FormattingEnabled = true;
        processorSettingsDropdown.Location = new Point(6, 30);
        processorSettingsDropdown.Name = "processorSettingsDropdown";
        processorSettingsDropdown.Size = new Size(412, 23);
        processorSettingsDropdown.TabIndex = 10;
        // 
        // processorSettingEditor
        // 
        processorSettingEditor.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
        processorSettingEditor.AutoScroll = true;
        processorSettingEditor.Location = new Point(6, 58);
        processorSettingEditor.Name = "processorSettingEditor";
        processorSettingEditor.Size = new Size(488, 271);
        processorSettingEditor.TabIndex = 9;
        // 
        // collectorBindFlow
        // 
        collectorBindFlow.AutoScroll = true;
        collectorBindFlow.Location = new Point(6, 178);
        collectorBindFlow.Name = "collectorBindFlow";
        collectorBindFlow.Size = new Size(221, 159);
        collectorBindFlow.TabIndex = 15;
        // 
        // processorFlow
        // 
        processorFlow.AutoScroll = true;
        processorFlow.Location = new Point(6, 6);
        processorFlow.Name = "processorFlow";
        processorFlow.Size = new Size(221, 151);
        processorFlow.TabIndex = 14;
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(6, 160);
        label1.Name = "label1";
        label1.Size = new Size(102, 15);
        label1.TabIndex = 13;
        label1.Text = "Bind To Collectors";
        // 
        // ledTab
        // 
        ledTab.Controls.Add(refreshLeds);
        ledTab.Controls.Add(addLedButton);
        ledTab.Controls.Add(ledFlow);
        ledTab.Location = new Point(4, 24);
        ledTab.Name = "ledTab";
        ledTab.Size = new Size(742, 346);
        ledTab.TabIndex = 2;
        ledTab.Text = "LEDs";
        ledTab.UseVisualStyleBackColor = true;
        // 
        // refreshLeds
        // 
        refreshLeds.Location = new Point(664, 5);
        refreshLeds.Name = "refreshLeds";
        refreshLeds.Size = new Size(75, 23);
        refreshLeds.TabIndex = 3;
        refreshLeds.Text = "Refresh";
        refreshLeds.UseVisualStyleBackColor = true;
        refreshLeds.Click += ReloadLeds_Click;
        // 
        // addLedButton
        // 
        addLedButton.Location = new Point(3, 5);
        addLedButton.Name = "addLedButton";
        addLedButton.Size = new Size(75, 23);
        addLedButton.TabIndex = 2;
        addLedButton.Text = "Add";
        addLedButton.UseVisualStyleBackColor = true;
        addLedButton.Click += AddLedButton_Click;
        // 
        // ledFlow
        // 
        ledFlow.AutoScroll = true;
        ledFlow.Location = new Point(3, 34);
        ledFlow.Name = "ledFlow";
        ledFlow.Size = new Size(736, 306);
        ledFlow.TabIndex = 0;
        // 
        // instanceStatusStrip
        // 
        instanceStatusStrip.Items.AddRange(new ToolStripItem[] { activeInstanceLabel, lastInstanceSaveLabel });
        instanceStatusStrip.Location = new Point(0, 489);
        instanceStatusStrip.Name = "instanceStatusStrip";
        instanceStatusStrip.Size = new Size(773, 22);
        instanceStatusStrip.TabIndex = 10;
        instanceStatusStrip.Text = "statusStrip1";
        // 
        // activeInstanceLabel
        // 
        activeInstanceLabel.Name = "activeInstanceLabel";
        activeInstanceLabel.Size = new Size(115, 17);
        activeInstanceLabel.Text = "Active Instance: N/A";
        // 
        // lastInstanceSaveLabel
        // 
        lastInstanceSaveLabel.Name = "lastInstanceSaveLabel";
        lastInstanceSaveLabel.Size = new Size(69, 17);
        lastInstanceSaveLabel.Text = "[Last Save: ]";
        // 
        // menuStrip1
        // 
        menuStrip1.Items.AddRange(new ToolStripItem[] { settingsMenu });
        menuStrip1.Location = new Point(0, 0);
        menuStrip1.Name = "menuStrip1";
        menuStrip1.Size = new Size(773, 24);
        menuStrip1.TabIndex = 11;
        menuStrip1.Text = "menuStrip1";
        // 
        // settingsMenu
        // 
        settingsMenu.Name = "settingsMenu";
        settingsMenu.Size = new Size(61, 20);
        settingsMenu.Text = "Settings";
        settingsMenu.Click += SettingsMenu_Click;
        // 
        // Main
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(773, 533);
        Controls.Add(instanceStatusStrip);
        Controls.Add(moduleEditTabs);
        Controls.Add(moduleBox);
        Controls.Add(groupBox1);
        Controls.Add(moduleStatusStrip);
        Controls.Add(menuStrip1);
        MainMenuStrip = menuStrip1;
        Name = "Main";
        Text = "Main";
        Load += Main_Load;
        moduleStatusStrip.ResumeLayout(false);
        moduleStatusStrip.PerformLayout();
        groupBox1.ResumeLayout(false);
        moduleBox.ResumeLayout(false);
        moduleEditTabs.ResumeLayout(false);
        moduleTab.ResumeLayout(false);
        moduleTab.PerformLayout();
        groupBox2.ResumeLayout(false);
        groupBox2.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)minDeviceUpdateTime).EndInit();
        collectorTab.ResumeLayout(false);
        collectorSettingsBox.ResumeLayout(false);
        processorTab.ResumeLayout(false);
        processorTab.PerformLayout();
        processorSettingsBox.ResumeLayout(false);
        ledTab.ResumeLayout(false);
        instanceStatusStrip.ResumeLayout(false);
        instanceStatusStrip.PerformLayout();
        menuStrip1.ResumeLayout(false);
        menuStrip1.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion
    private StatusStrip moduleStatusStrip;
    private ComboBox instanceDropdown;
    private Button createInstance;
    private Button loadInstance;
    private Button saveInstance;
    private Button deleteInstance;
    private GroupBox groupBox1;
    private GroupBox moduleBox;
    private ComboBox moduleDropdown;
    private Button deleteModule;
    private Button loadModule;
    private Button createModule;
    private Button saveModule;
    private TabControl moduleEditTabs;
    private TabPage collectorTab;
    private TabPage processorTab;
    private TabPage ledTab;
    private ToolStripStatusLabel activeModuleLabel;
    private Label label1;
    private FlowLayoutPanel collectorsFlow;
    private GroupBox collectorSettingsBox;
    private Button editCollectorSetting;
    private ComboBox collectorSettingsDropdown;
    private Panel collectorSettingEditor;
    private ToolTip onActionTooltip;
    private StatusStrip instanceStatusStrip;
    private ToolStripStatusLabel activeInstanceLabel;
    private ToolStripStatusLabel lastModuleSaveLabel;
    private ToolStripStatusLabel lastInstanceSaveLabel;
    private FlowLayoutPanel collectorBindFlow;
    private FlowLayoutPanel processorFlow;
    private GroupBox processorSettingsBox;
    private Button editProcessorSettings;
    private ComboBox processorSettingsDropdown;
    private Panel processorSettingEditor;
    private Button addLedButton;
    private FlowLayoutPanel ledFlow;
    private Button refreshLeds;
    private TabPage moduleTab;
    private Label label2;
    private NumericUpDown minDeviceUpdateTime;
    private CheckBox closeAfterModuleLaunch;
    private Button launchModule;
    private GroupBox groupBox2;
    private MenuStrip menuStrip1;
    private ToolStripMenuItem settingsMenu;
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Led;
using VKBLEDController.Config.Processors;
using VKBLEDController.Editor.Elements;

namespace VKBLEDController.Editor.GUI;
public partial class Main
{
    private void ReloadActiveLedData()
    {
        if (ActiveModule is null)
            return;

        var processors = ActiveModule.DataProcessors.Cast<ProcessorBase>().ToList();

        ledFlow.Invoke(() =>
        {
            ledFlow.Controls.Clear();
            foreach (var led in ActiveModule.Leds)
                ledFlow.Controls.Add(new LedControl(led, processors, OnDeleteLed));
        });
    }

    #region Events
    private void AddLedButton_Click(object sender, EventArgs e)
    {
        if (ActiveModule is null)
            return;

        var led = new LedEvent();
        ActiveModule.Leds.Add(led);

        ReloadActiveLedData();
    }

    private void OnDeleteLed(LedEvent led)
    {
        if (ActiveModule is null)
            return;

        ActiveModule.Leds.Remove(led);
        ReloadActiveLedData();
    }

    private void ReloadLeds_Click(object sender, EventArgs e)
        => ReloadActiveLedData();
    #endregion
}

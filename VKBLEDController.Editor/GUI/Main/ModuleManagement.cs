﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Instance;
using VKBLEDController.Config.Module;
using VKBLEDController.Config.Util;
using VKBLEDController.Editor.GUI.Popups;
using VKBLEDController.Editor.Properties;

namespace VKBLEDController.Editor.GUI;
public partial class Main
{
    private ModuleSettings? ActiveModule { get; set; }

    private async Task UpdateModulesAsync()
    {
        if (ActiveInstance is null)
            return;

        await ActiveInstance.LoadModulesAsync();

        moduleDropdown.Invoke(() =>
        {
            moduleDropdown.Items.Clear();
            foreach (var module in ActiveInstance.Modules)
                moduleDropdown.Items.Add(module.Name);
        });
    }

    private async Task SetActiveModuleAsync(ModuleSettings? module)
    {
        ActiveModule = module;

        moduleEditTabs.Invoke(() =>
        {
            moduleEditTabs.Enabled = module is not null;
            activeModuleLabel.Text = $"Active Module: {module?.Name ?? "N/A"}";
            moduleDropdown.SelectedIndex = moduleDropdown.Items.IndexOf(module?.Name);
            lastModuleSaveLabel.Text = string.Format(LAST_SAVE_FORMAT, module?.LastSave);

            if (module is not null)
            {
                // Apply module settings
                minDeviceUpdateTime.Value = (decimal)module.MinDeviceUpdateTime;
            }
        });

        if (module is null)
            return;

        await BuildActiveCollectors();
        ReloadCollectors();

        await BuildActiveProcessors();
        ReloadProcessors();

        ReloadActiveLedData();
    }

    #region Events
    public async void CreateModule_Click(object sender, EventArgs e)
    {
        if (ActiveInstance is null)
            return;

        using var popup = new InputStringDialogBox("Module Name", "Input New Module Name:");
        var res = popup.ShowDialog(this);

        if (res != DialogResult.OK)
            return;

        var module = new ModuleSettings(ActiveInstance, popup.textInput.Text);

        await module.SaveAsync();

        await UpdateInstancesAsync();
        await UpdateModulesAsync();
    }

    public async void LoadModule_Click(object sender, EventArgs e)
    {
        if (ActiveInstance is null)
            return;

        if (moduleDropdown.SelectedIndex == -1)
            return;

        if (ActiveModule is not null)
        {
            var res = MessageBox.Show($"The module: {ActiveModule.Name} is currently loaded. Loading a new module" +
                $" will result in all unsaved changes being lost.\n\nContinue to load new module?",
                "Load Module", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res != DialogResult.Yes)
                return;
        }

        var index = moduleDropdown.SelectedIndex;
        await UpdateModulesAsync();
        var module = ActiveInstance.Modules[index];

        await SetActiveModuleAsync(module);
    }

    public async void SaveModule_Click(object sender, EventArgs e)
    {
        if (ActiveModule is null)
            return;

        await ActiveModule.SaveAsync();

        Invoke(() =>
        {
            lastModuleSaveLabel.Text = SAVE_COMPLETE_MSG;

            _ = Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(2));

                lastModuleSaveLabel.Text = string.Format(LAST_SAVE_FORMAT, ActiveModule?.LastSave);
            });
        });
    }

    public async void DeleteModule_Click(object sender, EventArgs e)
    {
        if (ActiveInstance is null)
            return;

        if (moduleDropdown.SelectedIndex == -1)
            return;

        var module = ActiveInstance.Modules[moduleDropdown.SelectedIndex];

        var res = MessageBox.Show($"Are you sure you want to delete the module: {module.Name}?",
            "Delete Module", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
        if (res != DialogResult.Yes)
            return;

        if (module.Name.Equals(ActiveModule?.Name))
            await SetActiveModuleAsync(null);

        module.Delete();

        await UpdateModulesAsync();
    }

    private void MinDeviceUpdateTime_ValueChanged(object sender, EventArgs e)
    {
        if (ActiveModule is not null)
            ActiveModule.MinDeviceUpdateTime = (double)minDeviceUpdateTime.Value;
    }

    private void LaunchModule_Click(object sender, EventArgs e)
    {
        if (ActiveModule is null)
            return;

        SaveModule_Click(sender, e);

        var moduleLocation = ActiveModule._dirPath;

        var processInfo = new ProcessStartInfo()
        {
            Arguments = @$"-m ""{moduleLocation}""",
            WorkingDirectory = Environment.CurrentDirectory,
            FileName = $"{nameof(VKBLEDController)}.exe",
            UseShellExecute = true
        };

        _ = Process.Start(processInfo);

        if (closeAfterModuleLaunch.Checked)
            Environment.Exit(0);
    }
    #endregion
}

﻿namespace VKBLEDController.Editor.GUI.SettingsEdit;

partial class SettingsForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        saveAndClose = new Button();
        label1 = new Label();
        saveDataFolder = new TextBox();
        onPickFolder = new Button();
        folderPicker = new FolderBrowserDialog();
        SuspendLayout();
        // 
        // saveAndClose
        // 
        saveAndClose.Location = new Point(285, 88);
        saveAndClose.Name = "saveAndClose";
        saveAndClose.Size = new Size(107, 23);
        saveAndClose.TabIndex = 0;
        saveAndClose.Text = "Save And Close";
        saveAndClose.UseVisualStyleBackColor = true;
        saveAndClose.Click += SaveAndClose_Click;
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(12, 9);
        label1.Name = "label1";
        label1.Size = new Size(143, 15);
        label1.TabIndex = 1;
        label1.Text = "Save Data Folder Location";
        // 
        // saveDataFolder
        // 
        saveDataFolder.Location = new Point(12, 27);
        saveDataFolder.Name = "saveDataFolder";
        saveDataFolder.Size = new Size(334, 23);
        saveDataFolder.TabIndex = 2;
        // 
        // onPickFolder
        // 
        onPickFolder.Location = new Point(352, 27);
        onPickFolder.Name = "onPickFolder";
        onPickFolder.Size = new Size(40, 23);
        onPickFolder.TabIndex = 3;
        onPickFolder.Text = "Pick";
        onPickFolder.UseVisualStyleBackColor = true;
        onPickFolder.Click += OnPickFolder_Click;
        // 
        // Settings
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(404, 123);
        Controls.Add(onPickFolder);
        Controls.Add(saveDataFolder);
        Controls.Add(label1);
        Controls.Add(saveAndClose);
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "Settings";
        ShowIcon = false;
        ShowInTaskbar = false;
        SizeGripStyle = SizeGripStyle.Hide;
        StartPosition = FormStartPosition.CenterParent;
        Text = "Settings";
        TopMost = true;
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Button saveAndClose;
    private Label label1;
    private TextBox saveDataFolder;
    private Button onPickFolder;
    private FolderBrowserDialog folderPicker;
}
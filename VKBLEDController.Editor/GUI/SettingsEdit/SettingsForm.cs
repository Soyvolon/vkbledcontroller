﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.Editor.Properties;

namespace VKBLEDController.Editor.GUI.SettingsEdit;
public partial class SettingsForm : Form
{
    public SettingsForm()
    {
        InitializeComponent();

        Populate();
    }

    private void Populate()
    {
        saveDataFolder.Text = Settings.Default.InstanceDataPath;
    }

    #region Events
    private void SaveAndClose_Click(object sender, EventArgs e)
    {
        if (Directory.Exists(saveDataFolder.Text))
            Settings.Default.InstanceDataPath = saveDataFolder.Text;

        Settings.Default.Save();
        Close();
    }

    private void OnPickFolder_Click(object sender, EventArgs e)
    {
        var res = folderPicker.ShowDialog(this);
        if (res != DialogResult.OK)
        {
            return;
        }

        saveDataFolder.Text = folderPicker.SelectedPath;
    }
    #endregion
}

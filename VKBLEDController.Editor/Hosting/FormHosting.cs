﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Editor.Hosting;

public interface IFormStartup
{
    /// <summary>
    /// Handles configuration of services.
    /// </summary>
    /// <param name="services">The service collection for the host.</param>
    public void ConfigureServices(IServiceCollection services);

    /// <summary>
    /// Configures the host before startup.
    /// </summary>
    /// <param name="host">The <see cref="IHost"/> to configure.</param>
    public void Configure(IHost host);
}

public static class HostExtensions
{
    /// <summary>
    /// Runs the <see cref="IHost"/> with a windows form.
    /// </summary>
    /// <typeparam name="TStartup">The startup file type.</typeparam>
    /// <typeparam name="TForm">The form type.</typeparam>
    /// <param name="host">The <see cref="IHost"/>.</param>
    public static void RunForm<TStartup, TForm>(
        this IHost host)
        where TStartup : IFormStartup
        where TForm : Form
    {
        // Get the startup service ...
        var startup = host.Services.GetRequiredService<TStartup>();

        // ... configure it ...
        startup.Configure(host);

        // ... then get the form ...
        var form = host.Services.GetRequiredService<TForm>();

        // ... and run it.
        Application.Run(form);
    }
}

public static class HostBuilderExtensions
{
    private const string ConfigureServicesMethodName = "ConfigureServices";
    private const string StartMethodName = "Start";

    /// <summary>
    /// Specify the startup type to be used by the host.
    /// </summary>
    /// <remarks>
    /// Modified from <a href="https://stackoverflow.com/a/63603562/11682098">Stack Overflow</a>
    /// </remarks>
    /// <typeparam name="TStartup">The type containing an optional constructor with
    /// an <see cref="IConfiguration"/> parameter. The implementation should contain a public
    /// method named ConfigureServices with <see cref="IServiceCollection"/> parameter.</typeparam>
    /// <param name="hostBuilder">The <see cref="IHostBuilder"/> to initialize with TStartup.</param>
    /// <returns>The same instance of the <see cref="IHostBuilder"/> for chaining.</returns>
    public static IHostBuilder UseStartup<TStartup>(
        this IHostBuilder hostBuilder) where TStartup : class
    {
        // Invoke the ConfigureServices method on IHostBuilder...
        hostBuilder.ConfigureServices((ctx, serviceCollection) =>
        {
            // Find a method that has this signature: ConfigureServices(IServiceCollection)
            var cfgServicesMethod = typeof(TStartup).GetMethod(
                ConfigureServicesMethodName, new Type[] { typeof(IServiceCollection) });

            // Check if TStartup has a ctor that takes a IConfiguration parameter
            var hasConfigCtor = typeof(TStartup).GetConstructor(
                new Type[] { typeof(IConfiguration) }) != null;

            // create a TStartup instance based on ctor
            var startUpObj = hasConfigCtor ?
                (TStartup?)Activator.CreateInstance(typeof(TStartup), ctx.Configuration) :
                (TStartup?)Activator.CreateInstance(typeof(TStartup), null);

            if (startUpObj is not null)
            {
                serviceCollection.AddSingleton(startUpObj);

                // finally, call the ConfigureServices implemented by the TStartup object
                cfgServicesMethod?.Invoke(startUpObj, new object[] { serviceCollection });
            }
        });

        return hostBuilder;
    }
}

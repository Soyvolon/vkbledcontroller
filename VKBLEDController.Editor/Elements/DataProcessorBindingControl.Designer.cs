﻿namespace VKBLEDController.Editor.Elements;

partial class DataProcessorBindingControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        processorBinding = new GroupBox();
        toggleBindButton = new Button();
        processorBinding.SuspendLayout();
        SuspendLayout();
        // 
        // processorBinding
        // 
        processorBinding.Controls.Add(toggleBindButton);
        processorBinding.Location = new Point(3, 3);
        processorBinding.Name = "processorBinding";
        processorBinding.Size = new Size(176, 45);
        processorBinding.TabIndex = 0;
        processorBinding.TabStop = false;
        processorBinding.Text = "Processor Name";
        // 
        // toggleBindButton
        // 
        toggleBindButton.Location = new Point(99, 16);
        toggleBindButton.Name = "toggleBindButton";
        toggleBindButton.Size = new Size(71, 23);
        toggleBindButton.TabIndex = 0;
        toggleBindButton.Text = "Bind";
        toggleBindButton.UseVisualStyleBackColor = false;
        toggleBindButton.Click += ToggleEnableButton_Click;
        // 
        // DataProcessorBindingControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = SystemColors.Window;
        Controls.Add(processorBinding);
        Name = "DataProcessorBindingControl";
        Size = new Size(182, 51);
        processorBinding.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private GroupBox processorBinding;
    private Button toggleBindButton;
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VKBLEDController.Editor.Elements;
public partial class ActivatableControl : UserControl
{
    public string ActivatiableName { get; set; }
    public bool CollectorEnabled { get; set; } = false;
    public Action<string> OnEditRequested { get; set; }
    public Func<string, bool, Task<bool>> OnEnableChanged { get; set; }

    public ActivatableControl(string collectorName, bool enabled, 
        Func<string, bool, Task<bool>> onEnableToggled,
        Action<string> onEditRequested)
    {
        InitializeComponent();
        ActivatiableName
            = collector.Text
            = collectorName;

        SetEnabledData(enabled);

        OnEnableChanged = onEnableToggled;
        OnEditRequested = onEditRequested;
    }

    private void RequestEditButton_Click(object sender, EventArgs e)
        => OnEditRequested?.Invoke(ActivatiableName);

    private async void ToggleEnableButton_Click(object sender, EventArgs e)
        => await SetEnabledAsync(!CollectorEnabled);

    private async Task SetEnabledAsync(bool enabled)
    {
        var res = await OnEnableChanged.Invoke(ActivatiableName, enabled);

        if (res)
        {
            SetEnabledData(enabled);
        }
    }

    private void SetEnabledData(bool enabled)
    {
        toggleEnableButton.Text = enabled ? "Disable" : "Enable";
        toggleEnableButton.BackColor = enabled ? Color.LightSeaGreen : Color.LightCoral;
        CollectorEnabled = enabled;
        requestEditButton.Enabled = enabled;
    }
}

﻿namespace VKBLEDController.Editor.Elements;

partial class ActivatableControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        collector = new GroupBox();
        toggleEnableButton = new Button();
        requestEditButton = new Button();
        collector.SuspendLayout();
        SuspendLayout();
        // 
        // collector
        // 
        collector.Controls.Add(requestEditButton);
        collector.Controls.Add(toggleEnableButton);
        collector.Location = new Point(3, 3);
        collector.Name = "collector";
        collector.Size = new Size(176, 45);
        collector.TabIndex = 0;
        collector.TabStop = false;
        collector.Text = "Collector Name";
        // 
        // toggleEnableButton
        // 
        toggleEnableButton.Location = new Point(74, 16);
        toggleEnableButton.Name = "toggleEnableButton";
        toggleEnableButton.Size = new Size(53, 23);
        toggleEnableButton.TabIndex = 0;
        toggleEnableButton.Text = "Enable";
        toggleEnableButton.UseVisualStyleBackColor = false;
        toggleEnableButton.Click += ToggleEnableButton_Click;
        // 
        // requestEditButton
        // 
        requestEditButton.Location = new Point(133, 16);
        requestEditButton.Name = "requestEditButton";
        requestEditButton.Size = new Size(37, 23);
        requestEditButton.TabIndex = 1;
        requestEditButton.Text = "Edit";
        requestEditButton.UseVisualStyleBackColor = true;
        requestEditButton.Click += RequestEditButton_Click;
        // 
        // DataCollectorControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = SystemColors.Window;
        Controls.Add(collector);
        Name = "DataCollectorControl";
        Size = new Size(182, 51);
        collector.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private GroupBox collector;
    private Button toggleEnableButton;
    private Button requestEditButton;
}

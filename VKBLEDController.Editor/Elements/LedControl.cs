﻿using HidSharp;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using VKBLEDController.API.VKB;
using VKBLEDController.API.VKB.Config;
using VKBLEDController.API.VKB.Enum;
using VKBLEDController.Config.Led;
using VKBLEDController.Config.Led.Enum;
using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Util;
using VKBLEDController.GUI.Config.Wrappers;

namespace VKBLEDController.Editor.Elements;
public partial class LedControl : UserControl
{
    private const string COLOR_LABEL_FORMAT = "[{0}, {1}, {2}]";

    private LedEvent Led { get; init; }
    private EnumWrapper<ComparisonEnum> ComparisonOpWrapper { get; init; }
    private EnumWrapper<ColorMode> ColorModeWrapper { get; init; }
    private EnumWrapper<LedMode> LedModeWrapper { get; init; }
    private EnumWrapper<ColorMode> DisabledColorModeWrapper { get; init; }
    private EnumWrapper<LedMode> DisabledLedModeWrapper { get; init; }

    private List<VkbDevice> VkbDevices { get; init; }
    private List<ProcessorBase> Processors { get; init; }
    private Action<LedEvent> OnDeleteRequested { get; init; }

    public LedControl(LedEvent led, List<ProcessorBase> processors, Action<LedEvent> onDeleteRequested)
    {
        InitializeComponent();
        Led = led;

        ComparisonOpWrapper = new(compOp);
        ComparisonOpWrapper.ValueChangedEvent += (o, v) => Led.ComparisonOp = v;

        ColorModeWrapper = new(colorMode);
        ColorModeWrapper.ValueChangedEvent += (o, v) => Led.LedEnabledConfig.ColorMode = v;

        LedModeWrapper = new(ledMode);
        LedModeWrapper.ValueChangedEvent += (o, v) => Led.LedEnabledConfig.LedMode = v;

        DisabledColorModeWrapper = new(disabledColorMode);
        DisabledColorModeWrapper.ValueChangedEvent += (o, v) => Led.LedDisabledConfig.ColorMode = v;

        DisabledLedModeWrapper = new(disabledLedMode);
        DisabledLedModeWrapper.ValueChangedEvent += (o, v) => Led.LedDisabledConfig.LedMode = v;

        VkbDevices =
        [
            .. VkbUtils.GetVKBDevices().OrderBy(e => e.ProductId)
        ];
        Processors = processors;
        OnDeleteRequested = onDeleteRequested;

        Populate();
    }

    private void Populate()
    {
        // Name
        ledEventName.Text = Led.EventName;
        alwaysEnabled.Checked = Led.AlwaysEnabled;
        useDisabledLed.Checked = Led.UseDisabledLed;

        // Event config
        processor.Items.Clear();
        foreach (var p in Processors)
            processor.Items.Add(p.GetActivatableName() ?? "N/A");
        processor.SelectedIndex = processor.Items.IndexOf(Led.ProcessorToCheck);

        variable.Items.Clear();
        if (processor.SelectedIndex != -1)
            SetVariableDropdownValues();

        ComparisonOpWrapper.SetValue(Led.ComparisonOp);
        compareTo.Text = Led.CompareTo;
        isNumber.Checked = Led.ConvertToInt;

        // LED config
        ledId.Value = Led.LedEnabledConfig.Id;
        ColorModeWrapper.SetValue(Led.LedEnabledConfig.ColorMode);
        LedModeWrapper.SetValue(Led.LedEnabledConfig.LedMode);
        SetButtonImage(colorOne, colorOneLabel, Led.LedEnabledConfig.ColorOne);
        SetButtonImage(colorTwo, colorTwoLabel, Led.LedEnabledConfig.ColorTwo);
        enabledPriority.Value = Led.EnabledPriority;

        // LED disabled config
        disabledLedId.Value = Led.LedDisabledConfig.Id;
        DisabledColorModeWrapper.SetValue(Led.LedDisabledConfig.ColorMode);
        DisabledLedModeWrapper.SetValue(Led.LedDisabledConfig.LedMode);
        SetButtonImage(disabledColorOne, disabledColorOneLabel, Led.LedDisabledConfig.ColorOne);
        SetButtonImage(disabledColorTwo, disabledColorTwoLabel, Led.LedDisabledConfig.ColorTwo);
        disabledPriority.Value = Led.DisabledPriority;

        // Device config
        var deviceIndex = VkbDevices.FindIndex(e => e.VendorId == Led.DeviceData.VendorId
            && e.ProductId == Led.DeviceData.ProductId);

        forDevice.Items.Clear();
        foreach (var device in VkbDevices)
            forDevice.Items.Add($"{device.DeviceName} [vid: {device.VendorId}][pid: {device.ProductId}]");
        forDevice.SelectedIndex = deviceIndex;
    }

    private void SetVariableDropdownValues()
    {
        variable.Items.Clear();
        if (processor.SelectedIndex == -1)
            return;

        var selectedProcessor = Processors[processor.SelectedIndex];
        foreach (var varData in selectedProcessor.Settings.Variables)
            variable.Items.Add(varData.Name);
        variable.SelectedIndex = variable.Items.IndexOf(Led.ProcessorVariableToCheck);
    }

    private string? GetColorString()
    {
        var res = colorPicker.ShowDialog();
        if (res != DialogResult.OK)
            return null;

        var color = colorPicker.Color;
        string colorString = string.Format("#{0:X2}{1:X2}{2:X2}",
            color.R, color.G, color.B);

        return colorString;
    }

    #region Events
    private void IsNumber_CheckedChanged(object sender, EventArgs e)
        => Led.ConvertToInt = isNumber.Checked;


    private void Processor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (processor.SelectedIndex == -1)
            Led.ProcessorToCheck = "";
        else Led.ProcessorToCheck = (processor.Items[processor.SelectedIndex] as string)!;

        SetVariableDropdownValues();
    }

    private void Variable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (variable.SelectedIndex == -1)
            Led.ProcessorVariableToCheck = "";
        else Led.ProcessorVariableToCheck = (variable.Items[variable.SelectedIndex] as string)!;
    }

    private void CompareTo_TextChanged(object sender, EventArgs e)
        => Led.CompareTo = compareTo.Text;

    private void LedId_ValueChanged(object sender, EventArgs e)
        => Led.LedEnabledConfig.Id = (int)ledId.Value;

    private void ColorOne_Click(object sender, EventArgs e)
    {
        var colorString = GetColorString();

        if (colorString is null)
            return;

        Led.LedEnabledConfig.ColorOne = colorString;

        var ledHex = Led.LedEnabledConfig.ColorOne;

        SetButtonImage(colorOne, colorOneLabel, ledHex);
    }

    private void ColorTwo_Click(object sender, EventArgs e)
    {
        var colorString = GetColorString();

        if (colorString is null)
            return;

        Led.LedEnabledConfig.ColorTwo = colorString;

        var ledHex = Led.LedEnabledConfig.ColorTwo;
        SetButtonImage(colorTwo, colorTwoLabel, ledHex);
    }

    private void EnabledPriority_ValueChanged(object sender, EventArgs e)
        => Led.EnabledPriority = (int)enabledPriority.Value;

    private void ForDevice_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (forDevice.SelectedIndex == -1)
        {
            Led.DeviceData = new();
            return;
        }

        var device = VkbDevices[forDevice.SelectedIndex];
        Led.DeviceData.VendorId = device.VendorId;
        Led.DeviceData.ProductId = device.ProductId;
    }

    private void LedEventName_TextChanged(object sender, EventArgs e)
        => Led.EventName = ledEventName.Text;

    private void UseDisabledLed_CheckedChanged(object sender, EventArgs e)
        => Led.UseDisabledLed = useDisabledLed.Checked;

    private void AlwaysEnabled_CheckedChanged(object sender, EventArgs e)
        => Led.AlwaysEnabled = alwaysEnabled.Checked;

    private void Delete_Click(object sender, EventArgs e)
        => OnDeleteRequested.Invoke(Led);

    private void DisabledLedId_ValueChanged(object sender, EventArgs e)
        => Led.LedDisabledConfig.Id = (int)disabledLedId.Value;

    private void DisabledColorOne_Click(object sender, EventArgs e)
    {
        var colorString = GetColorString();

        if (colorString is null)
            return;

        Led.LedDisabledConfig.ColorOne = colorString;

        var ledHex = Led.LedDisabledConfig.ColorOne;
        SetButtonImage(disabledColorOne, disabledColorOneLabel, ledHex);
    }

    private void DisabledColorTwo_Click(object sender, EventArgs e)
    {
        var colorString = GetColorString();

        if (colorString is null)
            return;

        Led.LedDisabledConfig.ColorTwo = colorString;

        var ledHex = Led.LedDisabledConfig.ColorTwo;
        SetButtonImage(disabledColorTwo, disabledColorTwoLabel, ledHex);
    }

    private void DisabledPriority_ValueChanged(object sender, EventArgs e)
        => Led.DisabledPriority = (int)disabledPriority.Value;
    #endregion

    private static void SetButtonImage(Button button, Label label, string color)
    {
        int width = 20, height = 20;
        Bitmap bmp = new(width, height);
        using Graphics g = Graphics.FromImage(bmp);
        using SolidBrush b = new(ColorTranslator.FromHtml(color));
        g.FillRectangle(b, 0, 0, width, height);
        button.Image = bmp;

        var bytes = LedConfig.GetHexBits(color);
        label.Text = string.Format(COLOR_LABEL_FORMAT, bytes[0], bytes[1], bytes[2]);
    }
}

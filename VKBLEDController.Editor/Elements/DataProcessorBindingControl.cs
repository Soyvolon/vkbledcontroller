﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VKBLEDController.Editor.Elements;
public partial class DataProcessorBindingControl : UserControl
{
    public string CollectorName { get; set; }
    public bool CollectorEnabled { get; set; } = false;
    public Func<string, bool, Task<bool>> OnBindChanged { get; set; }

    public DataProcessorBindingControl(string collectorName, bool enabled, 
        Func<string, bool, Task<bool>> onBindToggled)
    {
        InitializeComponent();
        CollectorName
            = processorBinding.Text
            = collectorName;

        SetBoundData(enabled);

        OnBindChanged = onBindToggled;
    }

    private async void ToggleEnableButton_Click(object sender, EventArgs e)
        => await SetBindAsync(!CollectorEnabled);

    private async Task SetBindAsync(bool enabled)
    {
        var res = await OnBindChanged.Invoke(CollectorName, enabled);

        if (res)
        {
            SetBoundData(enabled);
        }
    }

    private void SetBoundData(bool enabled)
    {
        toggleBindButton.Text = enabled ? "Unbind" : "Bind";
        toggleBindButton.BackColor = enabled ? Color.LightSeaGreen : Color.LightCoral;
        CollectorEnabled = enabled;
    }
}

﻿namespace VKBLEDController.Editor.Elements;

partial class LedControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        tabControl1 = new TabControl();
        infoTab = new TabPage();
        useDisabledLed = new CheckBox();
        alwaysEnabled = new CheckBox();
        label3 = new Label();
        forDevice = new ComboBox();
        label7 = new Label();
        delete = new Button();
        ledEventName = new TextBox();
        triggerData = new TabPage();
        label6 = new Label();
        processor = new ComboBox();
        label5 = new Label();
        label4 = new Label();
        isNumber = new CheckBox();
        compareTo = new TextBox();
        variable = new ComboBox();
        compOp = new ComboBox();
        enabledLedData = new TabPage();
        label8 = new Label();
        enabledPriority = new NumericUpDown();
        label2 = new Label();
        label1 = new Label();
        colorTwo = new Button();
        colorOne = new Button();
        ledMode = new ComboBox();
        colorMode = new ComboBox();
        ledIdLabel = new Label();
        ledId = new NumericUpDown();
        disabledLedData = new TabPage();
        label12 = new Label();
        disabledPriority = new NumericUpDown();
        label9 = new Label();
        label10 = new Label();
        disabledColorTwo = new Button();
        disabledColorOne = new Button();
        disabledLedMode = new ComboBox();
        disabledColorMode = new ComboBox();
        label11 = new Label();
        disabledLedId = new NumericUpDown();
        colorPicker = new ColorDialog();
        colorOneLabel = new Label();
        colorTwoLabel = new Label();
        disabledColorTwoLabel = new Label();
        disabledColorOneLabel = new Label();
        tabControl1.SuspendLayout();
        infoTab.SuspendLayout();
        triggerData.SuspendLayout();
        enabledLedData.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)enabledPriority).BeginInit();
        ((System.ComponentModel.ISupportInitialize)ledId).BeginInit();
        disabledLedData.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)disabledPriority).BeginInit();
        ((System.ComponentModel.ISupportInitialize)disabledLedId).BeginInit();
        SuspendLayout();
        // 
        // tabControl1
        // 
        tabControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
        tabControl1.Controls.Add(infoTab);
        tabControl1.Controls.Add(triggerData);
        tabControl1.Controls.Add(enabledLedData);
        tabControl1.Controls.Add(disabledLedData);
        tabControl1.Location = new Point(3, 3);
        tabControl1.Name = "tabControl1";
        tabControl1.SelectedIndex = 0;
        tabControl1.Size = new Size(722, 78);
        tabControl1.TabIndex = 0;
        // 
        // infoTab
        // 
        infoTab.Controls.Add(useDisabledLed);
        infoTab.Controls.Add(alwaysEnabled);
        infoTab.Controls.Add(label3);
        infoTab.Controls.Add(forDevice);
        infoTab.Controls.Add(label7);
        infoTab.Controls.Add(delete);
        infoTab.Controls.Add(ledEventName);
        infoTab.Location = new Point(4, 24);
        infoTab.Name = "infoTab";
        infoTab.Size = new Size(714, 50);
        infoTab.TabIndex = 2;
        infoTab.Text = "Info";
        infoTab.UseVisualStyleBackColor = true;
        // 
        // useDisabledLed
        // 
        useDisabledLed.AutoSize = true;
        useDisabledLed.Location = new Point(430, 24);
        useDisabledLed.Name = "useDisabledLed";
        useDisabledLed.Size = new Size(116, 19);
        useDisabledLed.TabIndex = 13;
        useDisabledLed.Text = "Use Disabled LED";
        useDisabledLed.UseVisualStyleBackColor = true;
        useDisabledLed.CheckedChanged += UseDisabledLed_CheckedChanged;
        // 
        // alwaysEnabled
        // 
        alwaysEnabled.AutoSize = true;
        alwaysEnabled.Location = new Point(430, 5);
        alwaysEnabled.Name = "alwaysEnabled";
        alwaysEnabled.Size = new Size(119, 19);
        alwaysEnabled.TabIndex = 12;
        alwaysEnabled.Text = "Is Always Enabled";
        alwaysEnabled.UseVisualStyleBackColor = true;
        alwaysEnabled.CheckedChanged += AlwaysEnabled_CheckedChanged;
        // 
        // label3
        // 
        label3.AutoSize = true;
        label3.Location = new Point(203, 6);
        label3.Name = "label3";
        label3.Size = new Size(42, 15);
        label3.TabIndex = 11;
        label3.Text = "Device";
        // 
        // forDevice
        // 
        forDevice.DropDownStyle = ComboBoxStyle.DropDownList;
        forDevice.FormattingEnabled = true;
        forDevice.Location = new Point(203, 23);
        forDevice.Name = "forDevice";
        forDevice.Size = new Size(221, 23);
        forDevice.TabIndex = 10;
        forDevice.SelectedIndexChanged += ForDevice_SelectedIndexChanged;
        // 
        // label7
        // 
        label7.AutoSize = true;
        label7.Location = new Point(3, 6);
        label7.Name = "label7";
        label7.Size = new Size(71, 15);
        label7.TabIndex = 2;
        label7.Text = "Event Name";
        // 
        // delete
        // 
        delete.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
        delete.Location = new Point(593, 22);
        delete.Name = "delete";
        delete.Size = new Size(75, 23);
        delete.TabIndex = 1;
        delete.Text = "Delete LED";
        delete.UseVisualStyleBackColor = true;
        delete.Click += Delete_Click;
        // 
        // ledEventName
        // 
        ledEventName.Location = new Point(3, 24);
        ledEventName.Name = "ledEventName";
        ledEventName.Size = new Size(194, 23);
        ledEventName.TabIndex = 0;
        ledEventName.TextChanged += LedEventName_TextChanged;
        // 
        // triggerData
        // 
        triggerData.Controls.Add(label6);
        triggerData.Controls.Add(processor);
        triggerData.Controls.Add(label5);
        triggerData.Controls.Add(label4);
        triggerData.Controls.Add(isNumber);
        triggerData.Controls.Add(compareTo);
        triggerData.Controls.Add(variable);
        triggerData.Controls.Add(compOp);
        triggerData.Location = new Point(4, 24);
        triggerData.Name = "triggerData";
        triggerData.Padding = new Padding(3);
        triggerData.Size = new Size(714, 50);
        triggerData.TabIndex = 0;
        triggerData.Text = "Trigger";
        triggerData.UseVisualStyleBackColor = true;
        // 
        // label6
        // 
        label6.AutoSize = true;
        label6.Location = new Point(6, 3);
        label6.Name = "label6";
        label6.Size = new Size(58, 15);
        label6.TabIndex = 11;
        label6.Text = "Processor";
        // 
        // processor
        // 
        processor.DropDownStyle = ComboBoxStyle.DropDownList;
        processor.FormattingEnabled = true;
        processor.Location = new Point(6, 21);
        processor.Name = "processor";
        processor.Size = new Size(157, 23);
        processor.TabIndex = 10;
        processor.SelectedIndexChanged += Processor_SelectedIndexChanged;
        // 
        // label5
        // 
        label5.AutoSize = true;
        label5.Location = new Point(447, 3);
        label5.Name = "label5";
        label5.Size = new Size(71, 15);
        label5.TabIndex = 9;
        label5.Text = "Compare To";
        // 
        // label4
        // 
        label4.AutoSize = true;
        label4.Location = new Point(169, 3);
        label4.Name = "label4";
        label4.Size = new Size(48, 15);
        label4.TabIndex = 8;
        label4.Text = "Variable";
        // 
        // isNumber
        // 
        isNumber.AutoSize = true;
        isNumber.Location = new Point(632, 23);
        isNumber.Name = "isNumber";
        isNumber.Size = new Size(81, 19);
        isNumber.TabIndex = 7;
        isNumber.Text = "Is Number";
        isNumber.UseVisualStyleBackColor = true;
        isNumber.CheckedChanged += IsNumber_CheckedChanged;
        // 
        // compareTo
        // 
        compareTo.Location = new Point(447, 21);
        compareTo.Name = "compareTo";
        compareTo.Size = new Size(179, 23);
        compareTo.TabIndex = 6;
        compareTo.TextChanged += CompareTo_TextChanged;
        // 
        // variable
        // 
        variable.DropDownStyle = ComboBoxStyle.DropDownList;
        variable.FormattingEnabled = true;
        variable.Location = new Point(169, 21);
        variable.Name = "variable";
        variable.Size = new Size(185, 23);
        variable.TabIndex = 5;
        variable.SelectedIndexChanged += Variable_SelectedIndexChanged;
        // 
        // compOp
        // 
        compOp.DropDownStyle = ComboBoxStyle.DropDownList;
        compOp.FormattingEnabled = true;
        compOp.Location = new Point(360, 21);
        compOp.Name = "compOp";
        compOp.Size = new Size(81, 23);
        compOp.TabIndex = 4;
        // 
        // enabledLedData
        // 
        enabledLedData.Controls.Add(colorTwoLabel);
        enabledLedData.Controls.Add(colorOneLabel);
        enabledLedData.Controls.Add(label8);
        enabledLedData.Controls.Add(enabledPriority);
        enabledLedData.Controls.Add(label2);
        enabledLedData.Controls.Add(label1);
        enabledLedData.Controls.Add(colorTwo);
        enabledLedData.Controls.Add(colorOne);
        enabledLedData.Controls.Add(ledMode);
        enabledLedData.Controls.Add(colorMode);
        enabledLedData.Controls.Add(ledIdLabel);
        enabledLedData.Controls.Add(ledId);
        enabledLedData.Location = new Point(4, 24);
        enabledLedData.Name = "enabledLedData";
        enabledLedData.Padding = new Padding(3);
        enabledLedData.Size = new Size(714, 50);
        enabledLedData.TabIndex = 1;
        enabledLedData.Text = "On Enabled LED";
        enabledLedData.UseVisualStyleBackColor = true;
        // 
        // label8
        // 
        label8.AutoSize = true;
        label8.Location = new Point(492, 3);
        label8.Name = "label8";
        label8.Size = new Size(45, 15);
        label8.TabIndex = 10;
        label8.Text = "Priority";
        // 
        // enabledPriority
        // 
        enabledPriority.Location = new Point(492, 22);
        enabledPriority.Minimum = new decimal(new int[] { 100, 0, 0, int.MinValue });
        enabledPriority.Name = "enabledPriority";
        enabledPriority.Size = new Size(60, 23);
        enabledPriority.TabIndex = 9;
        enabledPriority.ValueChanged += EnabledPriority_ValueChanged;
        // 
        // label2
        // 
        label2.AutoSize = true;
        label2.Location = new Point(203, 3);
        label2.Name = "label2";
        label2.Size = new Size(61, 15);
        label2.TabIndex = 8;
        label2.Text = "LED Mode";
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(76, 3);
        label1.Name = "label1";
        label1.Size = new Size(70, 15);
        label1.TabIndex = 7;
        label1.Text = "Color Mode";
        // 
        // colorTwo
        // 
        colorTwo.ImageAlign = ContentAlignment.MiddleLeft;
        colorTwo.Location = new Point(411, 20);
        colorTwo.Name = "colorTwo";
        colorTwo.Size = new Size(75, 23);
        colorTwo.TabIndex = 6;
        colorTwo.Text = "Color 2";
        colorTwo.TextAlign = ContentAlignment.MiddleRight;
        colorTwo.UseVisualStyleBackColor = true;
        colorTwo.Click += ColorTwo_Click;
        // 
        // colorOne
        // 
        colorOne.ImageAlign = ContentAlignment.MiddleLeft;
        colorOne.Location = new Point(330, 20);
        colorOne.Name = "colorOne";
        colorOne.Size = new Size(75, 23);
        colorOne.TabIndex = 5;
        colorOne.Text = "Color 1";
        colorOne.TextAlign = ContentAlignment.MiddleRight;
        colorOne.UseVisualStyleBackColor = true;
        colorOne.Click += ColorOne_Click;
        // 
        // ledMode
        // 
        ledMode.DropDownStyle = ComboBoxStyle.DropDownList;
        ledMode.FormattingEnabled = true;
        ledMode.Location = new Point(203, 21);
        ledMode.Name = "ledMode";
        ledMode.Size = new Size(121, 23);
        ledMode.TabIndex = 4;
        // 
        // colorMode
        // 
        colorMode.DropDownStyle = ComboBoxStyle.DropDownList;
        colorMode.FormattingEnabled = true;
        colorMode.Location = new Point(76, 21);
        colorMode.Name = "colorMode";
        colorMode.Size = new Size(121, 23);
        colorMode.TabIndex = 3;
        // 
        // ledIdLabel
        // 
        ledIdLabel.AutoSize = true;
        ledIdLabel.Location = new Point(6, 3);
        ledIdLabel.Name = "ledIdLabel";
        ledIdLabel.Size = new Size(41, 15);
        ledIdLabel.TabIndex = 2;
        ledIdLabel.Text = "LED ID";
        // 
        // ledId
        // 
        ledId.Location = new Point(6, 22);
        ledId.Name = "ledId";
        ledId.Size = new Size(64, 23);
        ledId.TabIndex = 1;
        ledId.ValueChanged += LedId_ValueChanged;
        // 
        // disabledLedData
        // 
        disabledLedData.Controls.Add(disabledColorTwoLabel);
        disabledLedData.Controls.Add(disabledColorOneLabel);
        disabledLedData.Controls.Add(label12);
        disabledLedData.Controls.Add(disabledPriority);
        disabledLedData.Controls.Add(label9);
        disabledLedData.Controls.Add(label10);
        disabledLedData.Controls.Add(disabledColorTwo);
        disabledLedData.Controls.Add(disabledColorOne);
        disabledLedData.Controls.Add(disabledLedMode);
        disabledLedData.Controls.Add(disabledColorMode);
        disabledLedData.Controls.Add(label11);
        disabledLedData.Controls.Add(disabledLedId);
        disabledLedData.Location = new Point(4, 24);
        disabledLedData.Name = "disabledLedData";
        disabledLedData.Size = new Size(714, 50);
        disabledLedData.TabIndex = 3;
        disabledLedData.Text = "On Disabled LED";
        disabledLedData.UseVisualStyleBackColor = true;
        // 
        // label12
        // 
        label12.AutoSize = true;
        label12.Location = new Point(492, 3);
        label12.Name = "label12";
        label12.Size = new Size(45, 15);
        label12.TabIndex = 20;
        label12.Text = "Priority";
        // 
        // disabledPriority
        // 
        disabledPriority.Location = new Point(492, 22);
        disabledPriority.Minimum = new decimal(new int[] { 100, 0, 0, int.MinValue });
        disabledPriority.Name = "disabledPriority";
        disabledPriority.Size = new Size(60, 23);
        disabledPriority.TabIndex = 19;
        disabledPriority.ValueChanged += DisabledPriority_ValueChanged;
        // 
        // label9
        // 
        label9.AutoSize = true;
        label9.Location = new Point(203, 3);
        label9.Name = "label9";
        label9.Size = new Size(61, 15);
        label9.TabIndex = 18;
        label9.Text = "LED Mode";
        // 
        // label10
        // 
        label10.AutoSize = true;
        label10.Location = new Point(76, 3);
        label10.Name = "label10";
        label10.Size = new Size(70, 15);
        label10.TabIndex = 17;
        label10.Text = "Color Mode";
        // 
        // disabledColorTwo
        // 
        disabledColorTwo.ImageAlign = ContentAlignment.MiddleLeft;
        disabledColorTwo.Location = new Point(411, 20);
        disabledColorTwo.Name = "disabledColorTwo";
        disabledColorTwo.Size = new Size(75, 23);
        disabledColorTwo.TabIndex = 16;
        disabledColorTwo.Text = "Color 2";
        disabledColorTwo.TextAlign = ContentAlignment.MiddleRight;
        disabledColorTwo.UseVisualStyleBackColor = true;
        disabledColorTwo.Click += DisabledColorTwo_Click;
        // 
        // disabledColorOne
        // 
        disabledColorOne.ImageAlign = ContentAlignment.MiddleLeft;
        disabledColorOne.Location = new Point(330, 20);
        disabledColorOne.Name = "disabledColorOne";
        disabledColorOne.Size = new Size(75, 23);
        disabledColorOne.TabIndex = 15;
        disabledColorOne.Text = "Color 1";
        disabledColorOne.TextAlign = ContentAlignment.MiddleRight;
        disabledColorOne.UseVisualStyleBackColor = true;
        disabledColorOne.Click += DisabledColorOne_Click;
        // 
        // disabledLedMode
        // 
        disabledLedMode.DropDownStyle = ComboBoxStyle.DropDownList;
        disabledLedMode.FormattingEnabled = true;
        disabledLedMode.Location = new Point(203, 21);
        disabledLedMode.Name = "disabledLedMode";
        disabledLedMode.Size = new Size(121, 23);
        disabledLedMode.TabIndex = 14;
        // 
        // disabledColorMode
        // 
        disabledColorMode.DropDownStyle = ComboBoxStyle.DropDownList;
        disabledColorMode.FormattingEnabled = true;
        disabledColorMode.Location = new Point(76, 21);
        disabledColorMode.Name = "disabledColorMode";
        disabledColorMode.Size = new Size(121, 23);
        disabledColorMode.TabIndex = 13;
        // 
        // label11
        // 
        label11.AutoSize = true;
        label11.Location = new Point(6, 3);
        label11.Name = "label11";
        label11.Size = new Size(41, 15);
        label11.TabIndex = 12;
        label11.Text = "LED ID";
        // 
        // disabledLedId
        // 
        disabledLedId.Location = new Point(6, 22);
        disabledLedId.Name = "disabledLedId";
        disabledLedId.Size = new Size(64, 23);
        disabledLedId.TabIndex = 11;
        disabledLedId.ValueChanged += DisabledLedId_ValueChanged;
        // 
        // colorOneLabel
        // 
        colorOneLabel.AutoSize = true;
        colorOneLabel.Location = new Point(330, 3);
        colorOneLabel.Name = "colorOneLabel";
        colorOneLabel.Size = new Size(44, 15);
        colorOneLabel.TabIndex = 11;
        colorOneLabel.Text = "label13";
        // 
        // colorTwoLabel
        // 
        colorTwoLabel.AutoSize = true;
        colorTwoLabel.Location = new Point(411, 3);
        colorTwoLabel.Name = "colorTwoLabel";
        colorTwoLabel.Size = new Size(44, 15);
        colorTwoLabel.TabIndex = 12;
        colorTwoLabel.Text = "label13";
        // 
        // disabledColorTwoLabel
        // 
        disabledColorTwoLabel.AutoSize = true;
        disabledColorTwoLabel.Location = new Point(411, 3);
        disabledColorTwoLabel.Name = "disabledColorTwoLabel";
        disabledColorTwoLabel.Size = new Size(44, 15);
        disabledColorTwoLabel.TabIndex = 22;
        disabledColorTwoLabel.Text = "label13";
        // 
        // disabledColorOneLabel
        // 
        disabledColorOneLabel.AutoSize = true;
        disabledColorOneLabel.Location = new Point(330, 3);
        disabledColorOneLabel.Name = "disabledColorOneLabel";
        disabledColorOneLabel.Size = new Size(44, 15);
        disabledColorOneLabel.TabIndex = 21;
        disabledColorOneLabel.Text = "label13";
        // 
        // LedControl
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        Controls.Add(tabControl1);
        Name = "LedControl";
        Size = new Size(728, 85);
        tabControl1.ResumeLayout(false);
        infoTab.ResumeLayout(false);
        infoTab.PerformLayout();
        triggerData.ResumeLayout(false);
        triggerData.PerformLayout();
        enabledLedData.ResumeLayout(false);
        enabledLedData.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)enabledPriority).EndInit();
        ((System.ComponentModel.ISupportInitialize)ledId).EndInit();
        disabledLedData.ResumeLayout(false);
        disabledLedData.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)disabledPriority).EndInit();
        ((System.ComponentModel.ISupportInitialize)disabledLedId).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private TabControl tabControl1;
    private TabPage triggerData;
    private CheckBox isNumber;
    private TextBox compareTo;
    private ComboBox variable;
    private ComboBox compOp;
    private TabPage enabledLedData;
    private NumericUpDown ledId;
    private Label ledIdLabel;
    private ComboBox ledMode;
    private ComboBox colorMode;
    private ColorDialog colorPicker;
    private Button colorTwo;
    private Button colorOne;
    private Label label1;
    private Label label2;
    private Label label5;
    private Label label4;
    private Label label6;
    private ComboBox processor;
    private TabPage infoTab;
    private Button delete;
    private TextBox ledEventName;
    private Label label7;
    private TabPage disabledLedData;
    private Label label9;
    private Label label10;
    private Button disabledColorTwo;
    private Button disabledColorOne;
    private ComboBox disabledLedMode;
    private ComboBox disabledColorMode;
    private Label label11;
    private NumericUpDown disabledLedId;
    private Label label3;
    private ComboBox forDevice;
    private CheckBox alwaysEnabled;
    private CheckBox useDisabledLed;
    private Label label8;
    private NumericUpDown enabledPriority;
    private Label label12;
    private NumericUpDown disabledPriority;
    private Label colorTwoLabel;
    private Label colorOneLabel;
    private Label disabledColorTwoLabel;
    private Label disabledColorOneLabel;
}

# VKB LED Controller

> 1. Lights. 2. ???? 3. Action!

![status](https://gitlab.com/Soyvolon/vkbledcontroller/badges/development/pipeline.svg?ignore_skipped=true)

The VKB LED Controller is a project designed to add interactivity to the LEDs of a VKB device.

Also, a huge thanks to [tiberiusteng](https://github.com/tiberiusteng) over on github who created 
the [vkb-msfs-led](https://github.com/tiberiusteng/vkb-msfs-led) project. Without that project 
having instructions on how the LED configs are saved in a VKB device, I would never have
been able to get as far as I did with my own project.

## Releases

See [Releases](https://gitlab.com/Soyvolon/vkbledcontroller/-/releases) 
to download the latest release. The `.zip` file has a
`VKBLEController.Editor.exe` file that can be run to launch the
editor UI. The `VKBLEDController.exe` file is to run indiviual modules
once they have been configured.

## Usage Guide

The current version of the editor has some... kinks to work out still. Sometimes the editor
pages don't reset when you select differnet options, for example. When using the
GUI, note that most actions update without needed to be saved. However, if you don't
see an option you think should be there, try saving and loading the module then going back
to where you should see the option. It may just be there.

For information about the various parts of the GUI, 
[Read the Wiki](https://gitlab.com/Soyvolon/vkbledcontroller/-/wikis/home).

## Contributing

If you are looking to help programming a new way to collect or process data,
good news! The base classes/interfaces and attributes are all you really need
to get started. The application use reflection to find all collector/processor
classes and loads them into the GUI. It does the same for settings editors, which
also has an interface and base class.

The non-forms interfaces and classes can be found in the [Config](https://gitlab.com/Soyvolon/vkbledcontroller/-/tree/development/VKBLEDController.Config?ref_type=heads)
project, while the GUI interfaces/classes can be found in the [GUI.Config](https://gitlab.com/Soyvolon/vkbledcontroller/-/tree/development/VKBLEDController.GUI.Config?ref_type=heads)
project.

Bugfixes and QOL improvements are always welcome.

See [Contributing](https://gitlab.com/Soyvolon/vkbledcontroller/-/blob/development/CONTRIBUTING.md)
for more information.

<hr />

<a href='https://ko-fi.com/N4N42SM6N' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://storage.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

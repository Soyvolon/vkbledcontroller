# module
- data collectors
- led events
- data processors
- links

## data collectors
Things that collect data, like from an API or a rolling file folder.

## LED events
The settings that determine when an LED config should be added or removed from the device

LED settings are made up of a few parts:
1. The actual LED config
1. The threshold event (value > 500, for example)
1. The priority. An event with higher priority on the same LED will overwrite and existing, lower priority event.
1. The device that will be updated.

## Data processors
Systems to process data that is collected from a data collector, such as a JSON or CSV processor.
The processor needs to be able to split values, tell time, and keep a record of past values to know when
things have passed a thereshold.

## Links
A link is a group of collectors, processors, and events that all work together to
create an interactive link between the computer and its connected VKB devices.

# Workflow

1. The data collector gathers new data and sends it to the data processor
1. The data processor gets the data, runs necissary transforms on it, and sends out an update event
1. Each LED event checks the new data, compares it to its thresholds, and toggles itself on/off as needed
1. The processor finds the LEDs that are turned on, takes the highest priority items if there are duplicate LED IDs, and saves them to the device.

# Class Design
Each collector and/or processor inherits from a top level interface.
Each collector and/or processor have an attribute that determines if they are considered
published or not. If they do have the attribute, the application reads them in with reflection.
This is so additional modules can be added via .dll files from third party developers.
As there are source code, they cannot be edited locally by an end user, and must be develpoped
and re-packaged.

Cofigurations for the collector and/or processor are done via JSON files. Objects in the .dll that are
tagged as [ModuleSettings(Name = "settings group name")] are populated from json with default values to be edited later.
UI panels can be added to the module settings to customize the GUI for editing the various settings of
a module. LED event and link config pages cannot be modified.

LED events are stored in JSON files. Theses can be loaded by the application as templates for
further work or to be directly used. User modified events are saved elsewhere from events that
are packaged in a module.

Links are packaged as JSON files. Each link is a self-contained unit containing the collectors,
processors, and events that are needed to make a full link between a piece of software and
the controllers. A link may contain multiple of all three data types. A link can copied locally
and edited by a user.

# exe vs editor
The editor is the app that is used to do local changes (aka create and edit links and events, alongside configuring the data collector/processors).
The exe is an app that runs a single link. The exe can be launched by hand, but the editor also has a launch button. It takes the path to a Link.json
file as an input parameter. Without the link.json file, it wont run any links. Only a single link should be running at a time, or problems with the LEDs will
occor.
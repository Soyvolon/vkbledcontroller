﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Polling;
public class FilePollerLocationSettings
{
    /// <summary>
    /// The seconds between each check of a file.
    /// </summary>
    public double PollSeconds { get; set; } = 5.0;

    /// <summary>
    /// The path to the file to poll.
    /// </summary>
    public string PollFile { get; set; } = "";

    /// <summary>
    /// If true, the poller will always read all lines. If false,
    /// the poller only reads new lines.
    /// </summary>
    public bool ReadAllLines { get; set; } = true;

    /// <summary>
    /// If true, this is a rolling file.
    /// </summary>
    public bool RollingFile { get; set; } = false;

    /// <summary>
    /// The regex to match the file names for the rolling files.
    /// </summary>
    public string RollingFileRegex { get; set; } = "";

    /// <summary>
    /// Gets <see cref="PollFile"/> with environment variables expanded.
    /// </summary>
    /// <returns>A path with expanded env vars.</returns>
    public string GetExpandedPollFile()
        => Environment.ExpandEnvironmentVariables(PollFile);
}

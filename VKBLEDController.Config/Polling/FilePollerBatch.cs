﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Polling;
public class FilePollerBatch : IDisposable
{
    private bool disposedValue;
    private readonly FilePollerSettings _settings;

    private List<FilePoller> _pollers = [];

    public FilePollerBatch(FilePollerSettings settings)
    {
        _settings = settings;
    }

    public void Start(Action<string, List<string>> onFilePolled)
    {
        ObjectDisposedException.ThrowIf(disposedValue, this);

        foreach  (var location in _settings.PollingLocations)
        {
            var poller = new FilePoller(location);
            _pollers.Add(poller);

            poller.Start(onFilePolled);
        }
    }

    public bool IsDisposed()
        => disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {

            }

            foreach (var poll in _pollers)
                poll.Dispose();

            _pollers = null!;
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

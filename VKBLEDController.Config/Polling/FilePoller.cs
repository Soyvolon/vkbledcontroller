﻿using Microsoft.Extensions.DependencyInjection;

using Serilog;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;

namespace VKBLEDController.Config.Polling;

public class FilePoller : IDisposable
{
    private bool _disposed;
    private Timer? _timer;

    private FileStream? _pollFile;
    private StreamReader? _fileReader;

    private readonly FilePollerLocationSettings _settings;
    private readonly SemaphoreSlim _lock;

    public FilePoller(FilePollerLocationSettings settings)
    {
        _settings = settings;
        _lock = new SemaphoreSlim(1, 1);
    }

    public void Start(Action<string, List<string>> onFilePolled)
    {
        ObjectDisposedException.ThrowIf(_disposed, this);

        var pollFile = _settings.GetExpandedPollFile();
        pollFile = Path.GetFullPath(pollFile);

        string file;
        if (_settings.RollingFile)
        {
            if (!Directory.Exists(pollFile))
                pollFile = Path.GetDirectoryName(pollFile);

            if (!Directory.Exists(pollFile))
                return;

            file = GetLatestFile(pollFile);
        }
        else
        {
            file = pollFile;
        }

        OpenFile(file);

        var delay = TimeSpan.FromSeconds(_settings.PollSeconds);
        _timer = new(OnTimerCallback, onFilePolled, delay, delay);

        Log.Information("File Poller for {PollFile} started with settings: {@settings}", _settings.PollFile, _settings);
    }

    private string GetLatestFile(string pollFolder)
    {
        var regex = new Regex(_settings.RollingFileRegex);

        var folder = Path.GetFullPath(pollFolder);

        var fileList = Directory.GetFiles(folder)
            .Where(e =>
            {
                var res = regex.IsMatch(e);
                return res;
            });

        string latestWrite = "";
        DateTime latestWriteTime = new();
        foreach (var file in fileList)
        {
            var lastWrite = File.GetLastWriteTime(file);
            if (lastWrite > latestWriteTime)
            {
                latestWrite = file;
                latestWriteTime = lastWrite;
            }
        }

        return latestWrite;
    }

    private void OpenFile(string file)
    {
        _pollFile = new(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        _fileReader = new(_pollFile);
    }

    private async void OnTimerCallback(object? state)
    {
        if (_pollFile is null
            || _fileReader is null
            || _disposed)
            return;

        if (state is not Action<string, List<string>> onFilePolled)
            return;

        await _lock.WaitAsync();

        if (_settings.ReadAllLines)
            _pollFile.Seek(0, SeekOrigin.Begin);

        List<string> str = [];
        string? part;
        while ((part = await _fileReader.ReadLineAsync()) is not null)
            str.Add(part);

        if (_settings.RollingFile)
        {
            var dir = Path.GetDirectoryName(_pollFile.Name);
            var latest = GetLatestFile(dir!);

            // we have a new latest file, so open that.
            if (latest != _pollFile.Name)
            {
                _fileReader.Dispose();
                _pollFile.Dispose();

                OpenFile(latest);
            }

            // and read the lines from it
            while ((part = await _fileReader.ReadLineAsync()) is not null)
                str.Add(part);
        }

        onFilePolled.Invoke(_pollFile.Name, str);

        _lock.Release();
    }

    public void Stop()
    {
        _disposed = true;

        _fileReader?.Dispose();
        _pollFile?.Dispose();
        _timer?.Dispose();
    }

    public bool IsDisposed()
        => _disposed;

    public void Dispose()
        => Stop();
}

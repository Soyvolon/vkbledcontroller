﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Polling;

/// <summary>
/// Settings for the <see cref="FilePoller"/>.
/// </summary>
public class FilePollerSettings
{
    public List<FilePollerLocationSettings> PollingLocations { get; set; } = [];
}

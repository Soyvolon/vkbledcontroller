﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Led;

namespace VKBLEDController.Config.Processors.Settings;
public class ProcessorSettings
{
    public List<ProcessorVariableConfig> Variables { get; set; } = [];
    public List<string> ListenToCollectors { get; set; } = [];
}

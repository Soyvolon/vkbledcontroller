﻿using Json.Path;

using Serilog;

using System.Collections.Concurrent;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

using VKBLEDController.Config.Collectors;

namespace VKBLEDController.Config.Processors.DataPath;

public class DataProcessorBase : ProcessorBase
{
#pragma warning disable CS8618 // Populated via reflection.
    protected IDataSettingsBase DataSettings { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider adding the 'required' modifier or declaring as nullable.

    [JsonIgnore]
    public ConcurrentDictionary<string, string> Variables { get; set; } = [];

    protected void DataReceived(string source, string data,
        Func<string, string, string?> getRaw)
    {
        foreach (var pathCfg in DataSettings.GetConfigs())
        {
            if (!string.IsNullOrWhiteSpace(pathCfg.FromSourceRegex))
            {
                var reg = new Regex(pathCfg.FromSourceRegex);
                if (!reg.IsMatch(source))
                    continue;
            }

            // Check the data match to see if this bit of data needs
            // to be updated
            if (!string.IsNullOrWhiteSpace(pathCfg.DataMatch))
            {
                var reg = new Regex(pathCfg.DataMatch);

                if (!reg.IsMatch(data))
                    continue;

                Log.Verbose("Regex matched for {@data}", data);
            }

            try
            {
                var raw = getRaw(pathCfg.Expression, data);
                if (raw is null)
                    continue;

                Variables[pathCfg.VariableName] = raw;
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Failed to parse JSON data: {@data}", (source, data));
            }
        }
    }

    public virtual void DataReceived(string source, string data)
        => DataReceived(source, data, (expression, data) => null);

    public void DataReceived(string source, List<string> data)
        => data.ForEach((e) => DataReceived(source, e));

    public override void DataReceived(object sender, DataCollectedEventArgs args)
    {
        switch (args)
        {
            case DataCollectedEventArgs<string> strArgs:
                DataReceived(args.Source, strArgs.Data);
                break;
            case DataCollectedEventArgs<List<string>> listStringArgs:
                DataReceived(args.Source, listStringArgs.Data);
                break;
        }

        base.DataReceived(sender, args);
    }

    public override IReadOnlyDictionary<string, string> GetAllValues()
        => Variables;

    public override string? GetValue(string varName)
    {
        _ = Variables.TryGetValue(varName, out var res);
        return res;
    }
}

﻿namespace VKBLEDController.Config.Processors.DataPath;

public interface IDataSettingsBase
{
    public List<DataPathConfig> GetConfigs();
}

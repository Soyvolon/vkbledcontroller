﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Processors.DataPath;

namespace VKBLEDController.Config.Processors.DataPath.JSON;
public class JsonDataSettings : IDataSettingsBase
{
    public List<DataPathConfig> JPathConfigs { get; set; } = [];

    public List<DataPathConfig> GetConfigs()
        => JPathConfigs;
}

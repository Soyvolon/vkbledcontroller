﻿using Json.More;
using Json.Path;

using Serilog;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Processors.DataPath;

namespace VKBLEDController.Config.Processors.DataPath.JSON;

[DataProcessor("JSON Processor")]
public class JsonDataProcessor : DataProcessorBase
{
    [ModuleSettings("JSON Settings")]
    public JsonDataSettings? JsonSettings 
    { 
        get
        {
            if (DataSettings is not JsonDataSettings data)
                DataSettings = data = new JsonDataSettings();

            return data;
        }

        set => DataSettings = value ?? new();
    }

    public override void DataReceived(string source, string data)
        => DataReceived(source, data, ProcessJPath);

    private string? ProcessJPath(string expression, string data)
    {
        var path = JsonPath.Parse(expression);
        var node = JsonNode.Parse(data);

        var res = path.Evaluate(node);

        if (res.Matches?.Count < 1)
            return null;

        var raw = res.Matches![0].Value?.ToString() ?? "";

        return raw;
    }
}

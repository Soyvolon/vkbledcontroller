﻿using Json.Path;

using System.Text.Json.Nodes;
using System.Xml;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Processors.DataPath.JSON;

namespace VKBLEDController.Config.Processors.DataPath.XML;

[DataProcessor("XML Processor")]
public class XmlDataProcessor : DataProcessorBase
{
    [ModuleSettings("XML Settings")]
    public XmlDataSettings? JsonSettings
    {
        get
        {
            if (DataSettings is not XmlDataSettings data)
                DataSettings = data = new XmlDataSettings();

            return data;
        }

        set => DataSettings = value ?? new();
    }

    public override void DataReceived(string source, string data)
        => DataReceived(source, data, ProcessXPath);

    private string? ProcessXPath(string expression, string data)
    {
        var doc = new XmlDocument();
        doc.LoadXml(data);
        var res = doc.DocumentElement?.SelectSingleNode(expression);

        if (res is null)
            return null;

        return res.Value;
    }
}

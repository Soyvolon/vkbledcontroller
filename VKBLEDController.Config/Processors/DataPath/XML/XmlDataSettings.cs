﻿using VKBLEDController.Config.Processors.DataPath;

namespace VKBLEDController.Config.Processors.DataPath.XML;

public class XmlDataSettings : IDataSettingsBase
{
    public List<DataPathConfig> XPathConfigs { get; set; } = [];

    public List<DataPathConfig> GetConfigs()
        => XPathConfigs;
}

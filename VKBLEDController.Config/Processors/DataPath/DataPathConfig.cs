﻿namespace VKBLEDController.Config.Processors.DataPath;

public class DataPathConfig
{
    public string VariableName { get; set; } = "";
    public string DataMatch { get; set; } = "";
    public string FromSourceRegex { get; set; } = "";
    public string Expression { get; set; } = "";
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Util;

namespace VKBLEDController.Config.Processors;
public interface IDataProcessor : IActivatable
{
    public delegate void DataUpdatedEvent(IDataProcessor sender);
    public event DataUpdatedEvent DataUpdated;

    public void Initialize();
    public string? GetValue(string varName);
    public IReadOnlyDictionary<string, string> GetAllValues();
    public void DataReceived(object sender, DataCollectedEventArgs args);
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Led;
using VKBLEDController.Config.Processors.Settings;

namespace VKBLEDController.Config.Processors;
public abstract class ProcessorBase : IDataProcessor
{
    public event IDataProcessor.DataUpdatedEvent? DataUpdated;

    [ModuleSettings("Processor Settings")]
    public ProcessorSettings Settings { get; set; } = new();

    public virtual void DataReceived(object sender, DataCollectedEventArgs args)
    {
        _ = Task.Run(() => DataUpdated?.Invoke(this));
    }

    public abstract string? GetValue(string varName);
    public abstract IReadOnlyDictionary<string, string> GetAllValues();

    public virtual void Initialize()
    {
        
    }

}

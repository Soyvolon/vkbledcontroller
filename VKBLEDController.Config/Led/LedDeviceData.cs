﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Led;
public class LedDeviceData : IEquatable<LedDeviceData>
{
    public int VendorId { get; set; }
    public int ProductId { get; set; }

    public bool Equals(LedDeviceData? other)
        => GetHashCode().Equals(other?.GetHashCode());

    public override bool Equals(object? obj)
        => Equals(obj as LedDeviceData);

    public override int GetHashCode()
        => $"{VendorId}-{ProductId}".GetHashCode();
}

﻿using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using VKBLEDController.API.VKB.Config;
using VKBLEDController.Config.Led.Enum;
using VKBLEDController.Config.Processors;

namespace VKBLEDController.Config.Led;
public class LedEvent
{
    public string EventName { get; set; } = "";
    public int Priority { get; set; } = 0;
    public bool AlwaysEnabled { get; set; } = false;
    public bool UseDisabledLed { get; set; } = true;

    public LedConfig LedEnabledConfig { get; set; } = new();
    public int EnabledPriority { get; set; } = 1;
    public LedConfig LedDisabledConfig { get; set; } = new();
    public int DisabledPriority { get; set; } = -1;

    public string ProcessorToCheck { get; set; } = "";
    public string ProcessorVariableToCheck { get; set; } = "";
    public ComparisonEnum ComparisonOp { get; set; } = ComparisonEnum.Equal;
    public bool ConvertToInt { get; set; } = false;
    public string CompareTo { get; set; } = "";
    public LedDeviceData DeviceData { get; set; } = new();

    private bool _ledEnabled = false;
    [JsonIgnore]
    public bool LedEnabled
    {
        get
        {
            return _ledEnabled || AlwaysEnabled;
        }

        set
        {
            _ledEnabled = value;
        }
    }

    private Regex? _compToRegex;
    [JsonIgnore]
    private Regex CompToRegex
    {
        get
        {
            _compToRegex ??= new Regex(CompareTo);
            return _compToRegex;
        }
    }


    public void DataUpdated(IDataProcessor processor)
    {
        try
        {
            var value = processor.GetValue(ProcessorVariableToCheck);

            if (value is null)
            {
                LedEnabled = false;
                return;
            }

            if (ComparisonOp == ComparisonEnum.Regex)
            {
                LedEnabled = CompToRegex.IsMatch(value);
                return;
            }

            bool notFlag = ComparisonOp == ComparisonEnum.NotBitwiseFlag;
            bool flag = ComparisonOp == ComparisonEnum.BitwiseFlag
                || notFlag;

            if (ConvertToInt || flag)
            {
                if (int.TryParse(value, out var v1)
                    && int.TryParse(CompareTo, out var v2))
                {
                    if (flag)
                    {
                        bool present = BitsAreSet(v1, v2);
                        LedEnabled = notFlag ? !present : present;
                    }
                    else LedEnabled = ValueCompare(v1, v2);
                }
                else
                {
                    LedEnabled = false;
                }
            }
            else
            {
                LedEnabled = ValueCompare(value, CompareTo);
            }
        }
        catch(Exception ex)
        {
            Log.Warning(ex, "Failed to process data from {@processor}.", processor);
        }
    }

    private bool ValueCompare<T>(T v1, T v2)
        where T : IComparable<T>
        => ComparisonOp switch
        { 
            ComparisonEnum.Equal => v1.CompareTo(v2) == 0,
            ComparisonEnum.NotEqual => v1.CompareTo(v2) != 0,
            ComparisonEnum.LessThan => v1.CompareTo(v2) < 0,
            ComparisonEnum.LessThanOrEqual => v1.CompareTo(v2) <= 0,
            ComparisonEnum.GreaterThan => v1.CompareTo(v2) > 0,
            ComparisonEnum.GreaterThanOrEqual => v1.CompareTo(v2) >= 0,
            _ => false
        };

    public static bool BitsAreSet(int number, params int[] bitPositions)
    {
        var mask = 0;

        foreach (var bitPosition in bitPositions)
            mask |= (1 << bitPosition);

        return (number & mask) == mask;
    }
}

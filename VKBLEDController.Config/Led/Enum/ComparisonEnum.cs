﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Led.Enum;
public enum ComparisonEnum
{
    [Description("==")]
    Equal = 0,
    [Description("!=")]
    NotEqual = 1,
    [Description("<")]
    LessThan = 2,
    [Description("<=")]
    LessThanOrEqual = 3,
    [Description(">")]
    GreaterThan = 4,
    [Description(">=")]
    GreaterThanOrEqual = 5,

    [Description("F")]
    BitwiseFlag = 100,
    [Description("!F")]
    NotBitwiseFlag = 101,

    [Description("Regex")]
    Regex = 201,
}

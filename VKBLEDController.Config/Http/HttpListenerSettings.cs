﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Http;
public class HttpListenerSettings
{
    public List<HttpListenerInstanceSettings> Instances { get; set; } = [];
}

﻿namespace VKBLEDController.Config.Http;

public class HttpListenerInstanceSettings
{
    public string Name { get; set; } = "HTTP Listener";
    public int Port { get; set; }
}

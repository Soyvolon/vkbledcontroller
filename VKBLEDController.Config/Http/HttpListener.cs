﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Http;

public class HttpListener(HttpListenerInstanceSettings settings) : IDisposable
{
    private Task? HttpTask { get; set; }
    private WebApplication? WebApp { get; set; }
    private Action<string, string>? OnContentReceived { get; set; }

    public void Start(Action<string, string> onContent)
    {
        var builder = WebApplication.CreateBuilder();
        builder.WebHost.UseUrls("http://localhost:" + settings.Port);
        builder.Services.AddSerilog();

        WebApp = builder.Build();

        WebApp.MapPost("/data", RequestDelegate);

        OnContentReceived = onContent;

        HttpTask = Task.Run(async () =>
        {
            await WebApp.RunAsync();

        });

        Log.Information("HTTP Listener {Name} Started", settings.Name);
        Log.Information("HTTP endpoint is available at: http://localhost:{Port}/data", settings.Port);
    }

    private async Task RequestDelegate(HttpContext data)
    {
        Stream req = data.Request.Body;
        if (req.CanSeek)
            req.Seek(0, SeekOrigin.Begin);
        string strData = await (new StreamReader(req).ReadToEndAsync());

        string source = data.Request.Host.Value;
        if (!string.IsNullOrWhiteSpace(settings.Name))
            source = settings.Name;

        var sourceHeaders = data.Request.Headers.Where(e => e.Key == "VKBLED-Source");
        if (sourceHeaders.Any())
        {
            source = sourceHeaders.First().Value.FirstOrDefault() ?? source;
        }

        OnContentReceived?.Invoke(source, strData);

        Log.Information("Data received from {Source}: {strData} chars", source, strData.Length);

        data.Response.StatusCode = StatusCodes.Status202Accepted;
    }

    public void Dispose()
    {
        if (WebApp is not null)
            Task.Run(async () => await WebApp.DisposeAsync());

        if (HttpTask is not null)
        {
            HttpTask?.GetAwaiter().GetResult();
            HttpTask?.Dispose();
        }

        WebApp = null;
        HttpTask = null;

        Log.Debug("HTTP Listener {Name} at port {Port} stopped", settings.Name, settings.Port);
    }
}

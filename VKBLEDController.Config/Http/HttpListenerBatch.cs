﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Http;

public class HttpListenerBatch(HttpListenerSettings settings) : IDisposable
{
    private bool disposedValue;

    private List<HttpListener> Listeners { get; set; } = [];

    public bool IsDisposed()
        => disposedValue;

    public void Start(Action<string, string> onContent)
    {
        foreach(var instance in settings.Instances)
        {
            var listener = new HttpListener(instance);
            Listeners.Add(listener);
            listener.Start(onContent);
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                foreach (var listener in Listeners)
                    listener.Dispose();

                Listeners = null!;
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

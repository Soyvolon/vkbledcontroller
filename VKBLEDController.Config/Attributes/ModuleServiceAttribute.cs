﻿using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Attributes;

/// <summary>
/// Indicates a class should be provided as a service to other classes.
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class ModuleServiceAttribute : Attribute
{
    public ServiceLifetime Lifetime { get; set; }
}

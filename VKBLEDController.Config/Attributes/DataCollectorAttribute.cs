﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class DataCollectorAttribute(string name) : ActivatableAttribute(name)
{
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Attributes;

/// <summary>
/// Indicates a property is to be loaded from settings.
/// </summary>
/// <param name="name"></param>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class ModuleSettingsAttribute(string name) : Attribute
{
    public string Name { get; set; } = name;
}

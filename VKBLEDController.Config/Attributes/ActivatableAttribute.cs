﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace VKBLEDController.Config.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class ActivatableAttribute(string name) : Attribute
{
    public string Name { get; set; } = name;
}

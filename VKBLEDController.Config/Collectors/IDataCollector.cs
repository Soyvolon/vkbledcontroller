﻿using VKBLEDController.Config.Util;

namespace VKBLEDController.Config.Collectors;
public interface IDataCollector : IActivatable
{
    public delegate void DataCollectedEventHandler(object sender, DataCollectedEventArgs data);
    public event DataCollectedEventHandler DataCollected;

    public Task StartAsync();
    public void Stop();
}

public abstract class DataCollectedEventArgs(string source)
{
    public string Source { get; set; } = source;
}

public class DataCollectedEventArgs<T>(string source, T data) : DataCollectedEventArgs(source)
{
    public T Data { get; init; } = data;
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Polling;

namespace VKBLEDController.Config.Collectors.Files;

[DataCollector("Data File Collector")]
public class DataFileCollector : FileCollectorBase, IDataCollector
{
    public event IDataCollector.DataCollectedEventHandler? DataCollected;

    protected override void ParseUpdate(string source, List<string> lines)
    {
        StringBuilder sb = new();
        foreach(var line in lines)
            sb.AppendLine(line);

        DataCollected?.Invoke(this, new DataCollectedEventArgs<string>(source, sb.ToString()));
    }
}

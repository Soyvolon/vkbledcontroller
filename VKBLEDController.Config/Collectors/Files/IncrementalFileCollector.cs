﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Polling;

namespace VKBLEDController.Config.Collectors.Files;

[DataCollector("Incremental File Collector")]
public class IncrementalFileCollector : FileCollectorBase, IDataCollector
{
    public event IDataCollector.DataCollectedEventHandler? DataCollected;

    protected override void ParseUpdate(string source, List<string> lines)
        => DataCollected?.Invoke(this, new DataCollectedEventArgs<List<string>>(source, lines));
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Polling;

namespace VKBLEDController.Config.Collectors.Files;
public abstract class FileCollectorBase
{
    [ModuleSettings("File Poller Settings")]
    public FilePollerSettings PollerSettings { get; set; } = new();

    protected FilePollerBatch? Poller { get; set; }

    public virtual Task StartAsync()
    {
        if (Poller?.IsDisposed() ?? false)
            Poller = null;

        Poller ??= new(PollerSettings);

        Poller.Start(ParseUpdate);

        return Task.CompletedTask;
    }

    protected abstract void ParseUpdate(string source, List<string> lines);

    public virtual void Stop()
    {
        Poller?.Dispose();
        Poller = null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Http;
using VKBLEDController.Config.Polling;

namespace VKBLEDController.Config.Collectors.Http;

[DataCollector("Http Listener")]
public class HttpListenerCollector : IDataCollector
{
    [ModuleSettings("HTTP Listener Settings")]
    public HttpListenerSettings ListenerSettings { get; set; } = new();

    public event IDataCollector.DataCollectedEventHandler? DataCollected;

    protected HttpListenerBatch? Listeners { get; set; }
    
    public Task StartAsync()
    {
        if (!Listeners?.IsDisposed() ?? true)
            Listeners = null;

        Listeners ??= new(ListenerSettings);
        Listeners.Start(ParseUpdate);

        return Task.CompletedTask;
    }

    protected void ParseUpdate(string source, string content)
    {
        DataCollected?.Invoke(this, new DataCollectedEventArgs<string>(source, content));
    }

    public void Stop()
    {
        Listeners?.Dispose();
    }
}

﻿using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Collectors;
using VKBLEDController.Config.Instance;
using VKBLEDController.Config.Led;
using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Util;

namespace VKBLEDController.Config.Module;

/// <summary>
/// For holding information about various modules
/// </summary>
public class ModuleSettings
{
    public const string CFG_FILE_NAME = "module.json";
    public const string PROCESSOR_ROOT_FOLDER = "processors";
    public const string COLLECTOR_ROOT_FOLDER = "collectors";

    public string Name { get; set; } = "";
    public DateTime LastSave { get; set; } = DateTime.Now;
    public List<ObjectInstanceSettings> CollectorSettings { get; set; } = [];
    public List<ObjectInstanceSettings> ProcessorSettings { get; set; } = [];
    public List<LedEvent> Leds { get; set; } = [];

    /// <summary>
    /// The minimum time that must be spend between device updates.
    /// </summary>
    public double MinDeviceUpdateTime { get; set; } = 1.0;

    [JsonIgnore]
    public List<IDataCollector> DataCollectors { get; set; } = [];
    [JsonIgnore]
    public List<IDataProcessor> DataProcessors { get; set; } = [];
    [JsonIgnore]
    public string _dirPath = "";
    [JsonIgnore]
    public InstanceSettings? _parent;

    [JsonConstructor]
    public ModuleSettings() { }
    public ModuleSettings(InstanceSettings? parent, string name)
    {
        _parent = parent;
        if (_parent is not null)
            _dirPath = Path.Join(_parent._rootPath, _parent.Name, name);
        Name = name;
    }

    public async Task SaveAsync()
    {
        Directory.CreateDirectory(_dirPath);

        var moduleCfg = Path.Join(_dirPath, CFG_FILE_NAME);

        LastSave = DateTime.Now;

        using (var fs = new FileStream(moduleCfg, FileMode.Create))
            await JsonSerializer.SerializeAsync(fs, this, JsonUtils.serializerOptions);

        for (int i = 0; i < DataCollectors.Count; i++)
        {
            var root = Path.Join(_dirPath, COLLECTOR_ROOT_FOLDER, CollectorSettings[i].Name);
            await SaveObjectInstanceSettings(root, DataCollectors[i]);
        }

        for (int i = 0; i < DataProcessors.Count; i++)
        {
            var root = Path.Join(_dirPath, PROCESSOR_ROOT_FOLDER, ProcessorSettings[i].Name);
            await SaveObjectInstanceSettings(root, DataProcessors[i]);
        }
    }

    public async Task LoadCollectorsAsync()
    {
        for (int i = 0; i < CollectorSettings.Count; i++)
        {
            var root = Path.Join(_dirPath, COLLECTOR_ROOT_FOLDER, CollectorSettings[i].Name);
            var collector = (IDataCollector)(await LoadObjectInstanceSettings(root, CollectorSettings[i]))!;
            DataCollectors.Add(collector);
        }
    }

    public async Task LoadProcessorsAsync()
    {
        for (int i = 0; i < ProcessorSettings.Count; i++)
        {
            var root = Path.Join(_dirPath, PROCESSOR_ROOT_FOLDER, ProcessorSettings[i].Name);
            var processor = (IDataProcessor)(await LoadObjectInstanceSettings(root, ProcessorSettings[i]))!;
            DataProcessors.Add(processor);
        }
    }

    public async Task<bool> AddCollectorAsync(Type type, DataCollectorAttribute attr)
    {
        var obj = Activator.CreateInstance(type);
        if (obj is not IDataCollector activate)
            return false;

        DataCollectors.Add(activate);

        var root = Path.Join(_dirPath, COLLECTOR_ROOT_FOLDER, attr.Name);
        await SaveObjectInstanceSettings(root, activate);

        CollectorSettings.Add(new()
        {
            AssemblyQualifiedName = type.AssemblyQualifiedName ?? "",
            Name = attr.Name ?? ""
        });

        return true;
    }

    public bool RemoveCollector(Type type, DataCollectorAttribute attr)
    {
        var collector = DataCollectors
            .Where(e => e.GetType() == type)
            .FirstOrDefault();

        if (collector is not null)
            DataCollectors.Remove(collector);

        var typeAQN = type.AssemblyQualifiedName ?? "";
        var attrName = attr.Name ?? "";

        var collectorSettings = CollectorSettings
            .Where(e => e.AssemblyQualifiedName == typeAQN
                && e.Name == attrName)
            .FirstOrDefault();

        if (collectorSettings is not null)
            CollectorSettings.Remove(collectorSettings);

        var jsonPath = Path.Join(_dirPath, COLLECTOR_ROOT_FOLDER, attr!.Name);
        if (Directory.Exists(jsonPath))
            Directory.Delete(jsonPath, true);

        return true;
    }

    public async Task<bool> AddProcessorAsync(Type type, DataProcessorAttribute attr)
    {
        var obj = Activator.CreateInstance(type);
        if (obj is not IDataProcessor activate)
            return false;

        DataProcessors.Add(activate);

        var root = Path.Join(_dirPath, PROCESSOR_ROOT_FOLDER, attr.Name);
        await SaveObjectInstanceSettings(root, activate);

        ProcessorSettings.Add(new()
        {
            AssemblyQualifiedName = type.AssemblyQualifiedName ?? "",
            Name = attr.Name ?? ""
        });

        return true;
    }

    public bool RemoveProcessor(Type type, DataProcessorAttribute attr)
    {
        var processor = DataProcessors
            .Where(e => e.GetType() == type)
            .FirstOrDefault();

        if (processor is not null)
            DataProcessors.Remove(processor);

        var typeAQN = type.AssemblyQualifiedName ?? "";
        var attrName = attr.Name ?? "";

        var processorSettings = ProcessorSettings
            .Where(e => e.AssemblyQualifiedName == typeAQN
                && e.Name == attrName)
            .FirstOrDefault();

        if (processorSettings is not null)
            ProcessorSettings.Remove(processorSettings);

        var jsonPath = Path.Join(_dirPath, PROCESSOR_ROOT_FOLDER, attr!.Name);
        if (Directory.Exists(jsonPath))
            Directory.Delete(jsonPath, true);

        return true;
    }

    public void Delete()
    {
        Directory.Delete(_dirPath, true);
        _parent?.Modules.Remove(this);
    }

    public static async Task<ModuleSettings> LoadAsync(string dirPath, InstanceSettings? parent = null)
    {
        var moduleCfg = Path.Join(dirPath, CFG_FILE_NAME);
        if (!File.Exists(moduleCfg))
            throw new Exception($"Could not find {moduleCfg}");

        using var fs = new FileStream(moduleCfg, FileMode.Open);
        var module = await JsonSerializer.DeserializeAsync<ModuleSettings>(fs, JsonUtils.serializerOptions)
            ?? throw new Exception("Failed to parse module json data.");

        module._dirPath = dirPath;
        module._parent = parent;
        return module;
    }

    private static async Task<IActivatable?> LoadObjectInstanceSettings(string rootPath, ObjectInstanceSettings settings)
    {
        var type = Type.GetType(settings.AssemblyQualifiedName);
        if (type is null)
            return null;

        var obj = Activator.CreateInstance(type);
        if (obj is not IActivatable activate)
            return null;

        var pairs = GetSettingPropertyPairs(activate);

        foreach(var (prop, attr) in pairs)
        {
            var jsonPath = Path.Join(rootPath, attr!.Name + ".json");
            if (!File.Exists(jsonPath))
                continue;

            using var fs = new FileStream(jsonPath, FileMode.Open);
            var settingObj = await JsonSerializer.DeserializeAsync(fs, prop.PropertyType, JsonUtils.serializerOptions);

            prop.SetValue(activate, settingObj);
        }

        return activate;
    }

    private static async Task SaveObjectInstanceSettings(string rootPath, IActivatable activate)
    {
        Directory.CreateDirectory(rootPath);

        var pairs = GetSettingPropertyPairs(activate);

        foreach (var (prop, attr) in pairs)
        {
            var jsonPath = Path.Join(rootPath, attr!.Name + ".json");

            var propValue = prop.GetValue(activate);

            using var fs = new FileStream(jsonPath, FileMode.Create);
            await JsonSerializer.SerializeAsync(fs, propValue, prop.PropertyType, JsonUtils.serializerOptions);
        }
    }

    public static IEnumerable<(PropertyInfo prop, ModuleSettingsAttribute attr)> GetSettingPropertyPairs(IActivatable activate)
    {
        return activate.GetType().GetProperties()
            .Select(e => (prop: e, attr: e.GetCustomAttribute<ModuleSettingsAttribute>()!))
            .Where(e => e.attr is not null);
    }
}

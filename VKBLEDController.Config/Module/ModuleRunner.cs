﻿using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.API.VKB;
using VKBLEDController.API.VKB.Config;
using VKBLEDController.Config.Led;
using VKBLEDController.Config.Processors;
using VKBLEDController.Config.Util;

namespace VKBLEDController.Config.Module;

/// <summary>
/// Runs a module.
/// </summary>
public class ModuleRunner : IDisposable
{
    private bool disposedValue;

    private List<VkbDevice> VkbDevices = [];
    private readonly ModuleSettings _settings;

    private readonly Timer _executionTimer;
    private readonly TimeSpan _executionSeconds;
    private DateTime lastExecution;
    private bool timerActive;

    public ModuleRunner(ModuleSettings settings)
    {
        _settings = settings;

        _executionSeconds = TimeSpan.FromSeconds(_settings.MinDeviceUpdateTime);
        _executionTimer = new(UpdateDevices, null, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        lastExecution = DateTime.Now;
    }

    public async Task StartAsync()
    {
        Log.Information("Starting module runner.");

        VkbDevices = VkbUtils.GetVKBDevices();

        await _settings.LoadCollectorsAsync();
        await _settings.LoadProcessorsAsync();

        Log.Information("Collectors and Processors loaded.");

        // Hook the processors into the collectors ...
        foreach (ProcessorBase processor in _settings.DataProcessors.Cast<ProcessorBase>())
        {
            // Get the collectors ...
            var collectors = _settings.DataCollectors
                .Where(e => {
                    var name = e.GetActivatableName() ?? "";
                    return processor.Settings.ListenToCollectors
                        .Contains(name);
                });

            // ... wire up events ...
            foreach(var collector in collectors)
                collector.DataCollected += processor.DataReceived;

            // ... and the data updated event for the LEDs ...
            processor.DataUpdated += Processor_DataUpdated;
        }

        Log.Information("Connected {num} processor to collectors.", _settings.DataProcessors.Count);

        // Run the collectors ...
        foreach (var c in _settings.DataCollectors)
            await c.StartAsync();

        Log.Information("Started {num} collectors.", _settings.DataCollectors.Count);


        Log.Information("Startup complete.");
    }

    private void Processor_DataUpdated(IDataProcessor sender)
    {
        var senderName = sender.GetActivatableName();
        if (senderName is null)
            return;

        Log.Information("Updating LED data for processor: {name}", senderName);

        var leds = _settings.Leds
            .Where(e => e.ProcessorToCheck == senderName);

        foreach (var led in leds)
            led.DataUpdated(sender);

        TriggerDeviceUpdate();
    }

    private void TriggerDeviceUpdate()
    {
        if (timerActive)
            return;

        var curDiff = DateTime.Now - lastExecution;
        if (curDiff >= _executionSeconds)
        {
            UpdateDevices(null);
            return;
        }

        timerActive = true;
        _executionTimer.Change(_executionSeconds - curDiff, Timeout.InfiniteTimeSpan);
    }

    private void UpdateDevices(object? state)
    {
        lock (VkbDevices)
        {
            lastExecution = DateTime.Now;
            timerActive = false;

            Log.Debug("Updating LED devices.");

            Dictionary<LedDeviceData, List<(int priority, LedConfig led)>> groups = [];
            foreach (var led in _settings.Leds)
            {
                if (!groups.TryGetValue(led.DeviceData, out var group))
                {
                    group = [];
                    groups.Add(led.DeviceData, group);
                }

                if (led.LedEnabled)
                    group.Add((led.EnabledPriority, led.LedEnabledConfig));
                else if (led.UseDisabledLed) 
                    group.Add((led.DisabledPriority, led.LedDisabledConfig));
            }

            foreach (var device in VkbDevices)
                device.Leds.Clear();

            foreach (var group in groups)
            {
                var device = VkbDevices.Where(e => e.VendorId == group.Key.VendorId
                    && e.ProductId == group.Key.ProductId)
                    .FirstOrDefault();

                if (device is null)
                    continue;

                var order = group.Value.OrderByDescending(e => e.priority);

                foreach (var (priority, led) in order)
                {
                    if (!device.Leds.Any(e => e.Id == led.Id))
                        device.Leds.Add(led);
                }
            }

            foreach (var device in VkbDevices)
                device.SetLedConfig();

            Log.Debug("Updated LEDs for {count} devices.", VkbDevices.Count);
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

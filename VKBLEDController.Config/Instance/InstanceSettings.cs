﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

using VKBLEDController.Config.Module;
using VKBLEDController.Config.Util;

namespace VKBLEDController.Config.Instance;
public class InstanceSettings()
{
    public const string CFG_FILE_NAME = "instance.json";

    public string Name { get; set; } = "Default Instance";
    public DateTime LastSave { get; set; } = DateTime.Now;

    [JsonIgnore]
    public List<ModuleSettings> Modules { get; set; } = [];
    [JsonIgnore]
    public string _rootPath = "";

    public async Task SaveAsync()
    {
        var dirPath = Path.Join(_rootPath, Name);

        // Make sure the dir exists.
        Directory.CreateDirectory(dirPath);
        var instanceCfg = Path.Join(dirPath, CFG_FILE_NAME);

        LastSave = DateTime.Now;

        using (var fs = new FileStream(instanceCfg, FileMode.Create))
            await JsonSerializer.SerializeAsync(fs, this, JsonUtils.serializerOptions);

        foreach(var module in Modules)
            await module.SaveAsync();
    }

    public void Delete()
        => Directory.Delete(Path.Join(_rootPath, Name), true);

    public async Task LoadModulesAsync()
    {
        Modules.Clear();
        var dirPath = Path.Join(_rootPath, Name);

        foreach (var entry in Directory.GetFileSystemEntries(dirPath))
        {
            var moduleCfg = Path.Join(entry, ModuleSettings.CFG_FILE_NAME);
            if (!File.Exists(moduleCfg))
                continue;

            var module = await ModuleSettings.LoadAsync(entry, this);
            Modules.Add(module);
        }
    }

    public static async Task<InstanceSettings> LoadAsync(string dirPath)
    {
        var instanceCfg = Path.Join(dirPath, CFG_FILE_NAME);
        var rootPath = Path.GetDirectoryName(dirPath);

        if (!File.Exists(instanceCfg))
            throw new Exception("Instance file not found.");

        using var fs = new FileStream(instanceCfg, FileMode.Open);
        var instance = await JsonSerializer.DeserializeAsync<InstanceSettings>(fs, JsonUtils.serializerOptions) 
            ?? throw new Exception("Failed to load instance data from instance file.");

        instance._rootPath = rootPath ?? ".\\";
        return instance;
    }
}

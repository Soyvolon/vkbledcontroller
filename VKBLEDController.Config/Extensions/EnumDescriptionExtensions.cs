﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Extensions;
public static class EnumDescriptionExtensions
{
    /// <summary>
    /// Get the description of an enum.
    /// </summary>
    /// <typeparam name="T">The type of enum.</typeparam>
    /// <param name="value">The enum value.</param>
    /// <returns>The description.</returns>
    public static string GetDescription<T>(this T value) where T : System.Enum
    {
        // If no value, return blank.
        if (value is null) return string.Empty;

        // Otherwise, get the type ...
        var type = value.GetType();
        // ... and the name of the value ...
        var name = System.Enum.GetName(type, value);

        // ... if no name, return blank.
        if (name is null) return string.Empty;

        // ... otherwise get the description attribute of the
        // value.
        return type?.GetField(name)
            ?.GetCustomAttribute<DescriptionAttribute>()
            ?.Description ?? string.Empty;
    }

    /// <summary>
    /// Gets an enum from the description attribute value.
    /// </summary>
    /// <typeparam name="T">The enum type.</typeparam>
    /// <param name="value">The string value.</param>
    /// <returns>The enum whos description matches <see cref="value"/></returns>
    public static T? GetValueFromDescription<T>(this string value) where T : System.Enum
    {
        // For each value in the provided type ...
        foreach (T enumValue in System.Enum.GetValues(typeof(T)))
        {
            // ... set the compare value ...
            var compare = value;

            // ... and if the description matches ...
            if (GetDescription(enumValue) == compare)
                // ... then return the enum value.
                return enumValue;
        }

        // ... otherwise return the default.
        return default;
    }
}
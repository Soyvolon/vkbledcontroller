﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;
using VKBLEDController.Config.Instance;

namespace VKBLEDController.Config.Util;
public static class AssetLists
{
    public static List<(Type type, DataCollectorAttribute attr)> GetDataCollectors()
        => GetTypesWithAttribute<DataCollectorAttribute>();

    public static List<(Type type, DataProcessorAttribute attr)> GetDataProcessors()
        => GetTypesWithAttribute<DataProcessorAttribute>();

    public static List<(Type type, T attr)> GetTypesWithAttribute<T>()
        where T : Attribute
    {
        IEnumerable<(Type type, T attr)> dataCollectors = [];

        var assemblies = AppDomain.CurrentDomain.GetAssemblies();
        foreach (var assembly in assemblies)
        {
            var collectors = assembly.GetTypes()
                .SelectMany(e => e.GetCustomAttributes<T>()
                    .Select(a => (type: e, attr: a)))
                .Where(e => e.attr is not null);

            dataCollectors = dataCollectors.Concat(collectors);
        }

        return [.. dataCollectors];
    }

    public static async Task<List<InstanceSettings>> GetInstancesAsync(string dir, bool loadModules = true)
    {
        Directory.CreateDirectory(dir);

        List<InstanceSettings> settings = [];

        var entries = Directory.GetFileSystemEntries(dir);
        foreach(var entry in entries)
        {
            var configFile = Path.Join(entry, InstanceSettings.CFG_FILE_NAME);
            if (File.Exists(configFile))
            {
                var instance = await InstanceSettings.LoadAsync(entry);
                settings.Add(instance);

                if (loadModules)
                    await instance.LoadModulesAsync();
            }
        }

        return settings;
    }
}

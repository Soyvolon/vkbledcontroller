﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Util;
public static class JsonUtils
{
    public static readonly JsonSerializerOptions serializerOptions = new()
    {
        WriteIndented = true
    };
}

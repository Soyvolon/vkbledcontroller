﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Util;
public class ObjectInstanceSettings
{
    public string Name { get; set; } = "";
    public string AssemblyQualifiedName { get; set; } = "";
}

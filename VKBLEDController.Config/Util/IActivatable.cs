﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using VKBLEDController.Config.Attributes;

namespace VKBLEDController.Config.Util;
public interface IActivatable
{
}

public static class ActivatableExtensions
{
    public static string? GetActivatableName(this IActivatable activatable)
    {
        var name = activatable.GetType()
            .GetCustomAttribute<ActivatableAttribute>()
            ?.Name;

        return name;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKBLEDController.Config.Util;
public class AllowEqualsComparer : IComparer<int>
{
    public int Compare(int x, int y)
    {
        var comp = x.CompareTo(y);
        if (x == 0)
            return 1;
        return comp;
    }
}
